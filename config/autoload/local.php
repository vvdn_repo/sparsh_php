<?php
$envConfig = EnvConfig::getConfig();
return array(
     'db' => array(
         'username' => $envConfig["sqlusrname"],
         'password' => $envConfig["sqlpassword"],
     ),
 );