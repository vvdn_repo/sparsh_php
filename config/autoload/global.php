<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
$envConfig = EnvConfig::getConfig();
return array(
     'db' => array(
         'driver'         => 'Pdo',
         'dsn'            => 'mysql:dbname='.$envConfig["sqldbname"].';host='.$envConfig["sqlserver"],
         'driver_options' => array(
             PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
         ),
     ),
     'service_manager' => array(
         'factories' => array(
             'Zend\Db\Adapter\Adapter'
                     => 'Zend\Db\Adapter\AdapterServiceFactory',
         ),
     ),
    's3-sdk' => array(
        'bucket' => 'meru-firmware',
        'key' => 'AKIAIVVXHBVZDHVOQSUA',
        'secret' => '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',
    ),
 );