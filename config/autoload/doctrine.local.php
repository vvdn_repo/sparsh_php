<?PHP 
//config/autoload/doctrine.local.php
$envConfig = EnvConfig::getConfig();
return array(
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                    'params' => array(
                        'user' => $envConfig["sqlusrname"],
                        'password' => $envConfig["sqlpassword"],
                ),
            ),
        )
));
?>