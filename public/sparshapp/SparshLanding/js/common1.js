/*
++++++++++++++++++++++++++++++++++++++++++++++++++++++
AUTHOR : GUI TEAM COCHI
PROJECT :IntelliVision
VERSION : 1.1
++++++++++++++++++++++++++++++++++++++++++++++++++++++-*/

var rtsp_stream1="";
var stream_stat;
var stream_ip="";
var tg_status="";
var tg_ip="";
var clear;
var current_page;
var fw_versions;

var showHdideMenu=function(button)
{
 $(button).click(function() {

    var collapsedMargin = $('.mainpanel').css('margin-left');
    var collapsedLeft = $('.mainpanel').css('left');
     if(collapsedMargin === '220px' || collapsedLeft === '220px') {
      toggleMenu(-220,0);
    } else {
      toggleMenu(0,220);
    }

  });
}

var toggleMenu=function(marginLeft, marginMain)
{
    var emailList = ($(window).width() <= 768 && $(window).width() > 640)? 320 : 360;
    if($('.mainpanel').css('position') === 'relative')
	{
		$('.intelli-logo, .leftpanel').animate({left: marginLeft}, 'fast');
		$('.headerbar, .mainpanel').animate({left: marginMain}, 'fast');
		if($('body').css('overflow') == 'hidden')
		{
			$('body').css({overflow: ''});
		} 
		else 
		{
			$('body').css({overflow: 'hidden'});
		}
    }
	else
	{
		$('.intelli-logo, .leftpanel').animate({marginLeft: marginLeft}, 'fast');
		$('.headerbar, .mainpanel').animate({marginLeft: marginMain}, 'fast');
    }
}
var changeMenuColor=function()
{
$('.nav-intelli li > a').on('click', function() {
$('.nav-intelli').find('.active').removeClass('active');
$(this).parent().addClass("active");
}
)
}
  
var closeVisibleSubMenu=function()
{
    $('.leftpanel .nav-parent').each(function() {
        var t = jQuery(this);
        if(t.hasClass('nav-active')) {
            t.find('> ul').slideUp(200, function(){
               t.removeClass('nav-active');
            });
         }
    });
}  
  
var ToggleLeftMenu=function()
{
	$('.nav-parent > a').on('click', function() {
   
     var gran = $(this).closest('.nav');
     var parent = $(this).parent();
     var sub = parent.find('> ul');	 
     if(sub.is(':visible'))
	 {
       sub.slideUp(200);
       if(parent.hasClass('nav-active')) { parent.removeClass('nav-active');
	   

	   }
     }
	 else 
	 {
       $(gran).find('.children').each(function() {
         $(this).slideUp();
       });

       sub.slideDown(200);
       if(!parent.hasClass('active')) { parent.addClass('nav-active'); }
     }
    return false;
   });
}

var configOpen=function(pagename,holder,i)
{
	$(holder).load(pagename);
    current_page=pagename;
	makeActive(i);
	if(pagename!="network_lte.html")
	{
		clearInterval(clear);
	}
};

function makeActive(i)
{
    for (j = 1; j <= 30; j++)
    {
        if (i == j)
        {
            //$('#a' + i).css({'color':'#464f63',"border-radius":"50px","border-color":"#657390"});
			$('#a' + i).css('color', '#000000');
			//$('li#b' + i).css('color', '#a0a8b9');
		}
        else
        {
            $('#a' + j).css('color', '#657390');
			//$('li#b' + j).css('background-color', '#657390');
		}
    }
}




