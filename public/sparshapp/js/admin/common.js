/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//initSlider('#DIV_ID',MIN,MAX,STEP,SLIDE,STOP)
var initSlider = function(id, min, max, step, value, slide_c, stop_c)
{
    $(id).slider(
            {
                min: parseInt(min),
                max: parseInt(max),
                step: parseInt(step),
                value: parseInt(value),
                /*create: function(event, ui) {
                 if(create_c)
                 create_c(id,event,ui);
                 },*/
                slide: function(event, ui) {
                    if (slide_c)
                        slide_c(id, event, ui);
                },
                change: function(event, ui) {
                    if (slide_c)
                        slide_c(id, event, ui);
                },
                stop: function(event, ui) {
                    if (stop_c)
                        stop_c(id, event, ui);
                }
            });
};
var slider_get = function(id)
{
    var val = $(id).slider("value");
    return parseInt(val);
};
var showSliderval = function(id, event, ui)
{
    var val = slider_get(id);
    id = id + "_val";
    $(id).text(val);
};
function ResizePlayers()
{
    var pWidth = $("div#videoC").width() - 20;
    var pHeight = $("div#videoC").height() - 70;
    $("#player").css({"width": pWidth, "height": pHeight});
}




var configOpen = function(pagename, holder)
{
    $(holder).load(pagename);
};

function openPage(div1, div2, content)
{
    $(div1).fadeIn("slow");
    $(div2).load(content);
}
function closeDiv(div)
{
    $(div).fadeOut("slow");
    $("object#player,#player").css("visibility", "inherit");
}
var DeviceTable;
var callDevice = function(url, type, data, callback)
{
   // alert("here");
    var path = window.location.pathname;
    var res = path.split("/");
    DeviceTable = $('#DeviceTable').dataTable({
        responsive: true,
        "bProcessing": true,
        "iDisplayLength": 10,
        "sAjaxSource": "../device/list",
        aoColumns: [
            {"sTitle": 'Name', mData: "deviceName"},
            //{"sTitle": 'Type', mData: "deviceType"},
            {"sTitle": 'Mac Address', mData: "deviceMac"},
           // {"sTitle": 'IP Address', mData: "deviceIP"},
	   // {"sTitle": 'FW Version', mData: "deviceFW"},
            {
                "sTitle": 'Action',
                mData: null,
                bSortable: false,
                mRender: function(o) {
                    return '<a class="btn btn-xs btn-primary" onclick="editDevcie(' + o.deviceId + ',' + '\'' + o.deviceName + '\'' + ',' + '\'' + o.deviceToken + '\'' + ')"><span class="mobile-hide"><i class="glyphicon glyphicon-pencil"></i>Edit</span></a>&nbsp;&nbsp;'
                            + '<a class="btn btn-xs btn-primary" onclick="deviceConfiguration(' + o.deviceId + ')"><span class="mobile-hide"><i class="glyphicon glyphicon-cog"></i>Configuration</span></a>&nbsp;&nbsp;'
                            + '<a class="btn btn-xs btn-primary" onclick="updateFw(' + o.deviceId + ')"><span class="mobile-hide"><i class="glyphicon glyphicon-eye-open"></i>Upgrade Firmware</span></a>&nbsp;&nbsp;'
                            + '<a class="btn btn-xs btn-primary" onclick="reset(' + o.deviceId + ')"><span class="mobile-hide"><i class="glyphicon glyphicon-wrench"></i>Reset</span></a>';
                }
            }
        ],
        "fnInitComplete": function(oSettings) {
            $('.dataTables_filter label').append("<i class='glyphicon glyphicon-search' style='position:relative; right:20px;'></i>");
        }
    });
};
/*** Edit Device ****/
var editDevcie = function(id, name, token) {
    $(".device_success_msg,.device_failure_msg").addClass('hide');
    $(".success_msg,.failure_msg").text('');
    $('#deviceModal').modal({backdrop: 'static'});
    $("#device_name").val(name);
    $("#device_token").val(token);
    $("#device_id").val(id);
    $(".device_name_errmsg").hide();
};
var validateAndSaveDevice = function() {
    var device_name = $("#device_name").val();
    var device_id = $("#device_id").val();
    var device_token = $("#device_token").val();
    if (device_name == '') {
        $(".device_name_errmsg").show();
        $("#savedevice_btn").attr('disabled', 'disabled');
        $(".device_name_errmsg").text("Device name cannot be empty!!");
    }
    else {
        var device_data = {
            'device_name': device_name,
            'device_id': device_id,
            'device_token': device_token
        };
        $.blockUI({message: '<img src="' + APP_PATH + '/img/ajax-loader.gif" />'});
        $.ajax({
            url: "../device/edit-device",
            type: 'POST',
            data: JSON.stringify(device_data),
            success: function(result) {
                $.unblockUI();
                if (result.status == true) {
                    $(".device_success_msg").removeClass('hide');
                    $(".success_msg").text(result.message);
                }
                else {
                    $(".device_failure_msg").removeClass('hide');
                    $(".failure_msg").text(result.message);
                }
                DeviceTable.fnReloadAjax();
            },
            error: function(error) {
                $.unblockUI();
                $(".device_failure_msg").removeClass('hide');
                $(".failure_msg").text('Failed to edit device name');
            }
        });
        $('#deviceModal').modal("toggle");
    }
};
var clearValidationDevice = function() {
    $(".device_name_errmsg").hide();
    $("#savedevice_btn").removeAttr('disabled');
};
/*** End of Edit Device ***/

/*** Device Configuration ****/
var deviceConfiguration = function(id) {
    $(".device_success_msg,.device_failure_msg").addClass('hide');
    $(".success_msg,.failure_msg").text('');
    $('.device_config_fields').bootstrapSwitch();
    $("#device_id").val(id);
    var device_id = {
        'device_id': id
    };
    var motion_detection, cloud_recording, cube_recording, audio_enable, hd_enable;
    $.ajax({
        url: "../device/view-config",
        type: 'POST',
        data: JSON.stringify(device_id),
        success: function(result) {
            result = JSON.parse(result);
            motion_detection = result.response['motion-detection'];
            cloud_recording = result.response['cloud-recording'];
            cube_recording = result.response['cube-recording'];
            audio_enable = result.response['audio-enable'];
            hd_enable = result.response['hd-enable'];
            $("#motion_detection").bootstrapSwitch('state', parseInt(motion_detection));
            $("#cube_recording").bootstrapSwitch('state', parseInt(cube_recording));
            $("#cloud_recording").bootstrapSwitch('state', parseInt(cloud_recording));
            $("#audio_enable").bootstrapSwitch('state', parseInt(audio_enable));
            $("#hd_enable").bootstrapSwitch('state', parseInt(hd_enable));
        },
        error: function(error) {
            $(".device_failure_msg").removeClass('hide');
            $(".failure_msg").text('Failed to get device configuration');
        }
    });
    $('#deviceConfgModal').modal({backdrop: 'static'});
};
var validateAndSaveDeviceConfg = function() {
    var motion_detection = $("#motion_detection").is(":checked") ? 1 : 0;
    var cloud_recording = $("#cloud_recording").is(":checked") ? 1 : 0;
    var cube_recording = $("#cube_recording").is(":checked") ? 1 : 0;
    var audio_enable = $("#audio_enable").is(":checked") ? 1 : 0;
    var hd_enable = $("#hd_enable").is(":checked") ? 1 : 0;
    var device_config_data = {
        'commandId': 12,
        'command': "update-config",
        'device_id': parseInt($("#device_id").val()),
        'params': {
            'motion-detection': motion_detection,
            'cloud-recording': cloud_recording,
            'cube-recording': cube_recording,
            'audio-enable': audio_enable,
            'hd-enable': hd_enable
        }
    };
    $.blockUI({message: '<img src="' + APP_PATH + '/img/ajax-loader.gif" />'});
    $.ajax({
        url: "../device/update-config",
        type: 'POST',
        data: JSON.stringify(device_config_data),
        success: function(result) {
            $.unblockUI();
            if (result.status == true) {
                $(".device_success_msg").removeClass('hide');
                $(".success_msg").text(result.message);
            }
            else {
                $(".device_failure_msg").removeClass('hide');
                $(".failure_msg").text(result.message);
            }
            DeviceTable.fnReloadAjax();
        },
        error: function(error) {
            $.unblockUI();
            $(".device_failure_msg").removeClass('hide');
            $(".failure_msg").text('Failed to Update device configuration');
        }
    });
    $('#deviceConfgModal').modal("toggle");
};
/*** End of Device Configuraion ***/

/*** Scan Sensor ***/
var updateFw = function(id) {
    $(".device_success_msg,.device_failure_msg").addClass('hide');
    $(".success_msg,.failure_msg").text('');
    $('.device_config_fields').bootstrapSwitch();
    $("#device_id").val(id);
    asd =String(id);
    var scan_sensor = {
        'commandId': 11,
        'command': "upgrade-fw",
        'device_id': asd
    };
    $.blockUI({message: '<img src="' + APP_PATH + '/img/ajax-loader.gif" />'});
    $.ajax({
        url: "../update-fw",
        type: 'POST',
        data: JSON.stringify(scan_sensor),
        success: function(result) {
            if (result.status == true) {
                setTimeout(function() {
                    sensorResult(result.requestId, id);
                }, parseInt(result.interval * 1000));
            }
            else {
                $(".device_failure_msg").removeClass('hide');
                $(".failure_msg").text(result.message);
                $.unblockUI();
            }
        },
        error: function(error) {
            $(".device_failure_msg").removeClass('hide');
            $(".failure_msg").text('Failed to Upgrade');
            $.unblockUI();
        }
    });
};
var sensorResult = function(req_id, device_id) {
    $(".pairSensor_success_msg,.pairSensor_failure_msg").addClass('hide');
    $(".pair_success_msg,.pair_failure_msg").text('FW Upgrade started');
   
};

/*** End of Scan Sensor ***/

/*** Reset Device ***/
var reset = function(id) {
    var reset_device = {"commandId": "91", "command": "restore-config", "device_id": id};
    bootbox.dialog({
        message: "Do you want to reset device?",
        title: "Factory Reset",
        className: "modal-delete",
        buttons: {
            cancel: {
                label: "Cancel",
                className: "btn-default",
                callback: function() {
                }
            },
            ok: {
                label: 'OK',
                className: "btn-primary",
                callback: function() {
                    $.blockUI({message: '<img src="' + APP_PATH + '/img/ajax-loader.gif" />'});
                    $.ajax({
                        url: "../device/restore-config",
                        type: 'POST',
                        data: JSON.stringify(reset_device),
                        success: function(result) {
                            $(".device_failure_msg").removeClass('hide');
                            $(".failure_msg").text(result.message);
                            $.unblockUI();
                        },
                        error: function(error) {
                            $(".device_failure_msg").removeClass('hide');
                            $(".failure_msg").text('Failed to reset device');
                            $.unblockUI();
                        }
                    });
                }
            }
        }
    });
};
/*** End of Reset Device ***/

/*** close message box ***/
var closeMsg = function(type) {
    if (type == 'success') {
        $(".device_success_msg,.pairSensor_success_msg").addClass('hide');
        $(".success_msg,.pair_success_msg").text('');
    }
    else {
        $(".device_failure_msg,.pairSensor_failure_msg").addClass('hide');
        $(".failure_msg,.pair_failure_msg").text('');
    }
};
/*** End of close message box ***/


    //////////////////   Admin List Users////////////////////////////////


var callUser = function(url, type, data, callback)
{alert("hereee");
    UserTable = $('#DeviceTable').dataTable({
        responsive: true,
        "bProcessing": true,
        "iDisplayLength": 10,
        "sAjaxSource": "../admin/getusers",
        aoColumns: [
            {"sTitle": 'Name', mData: "userName"},
            {"sTitle": 'Email', mData: "userEmail"},
           // {"sTitle": 'Mac Address', mData: "deviceMac"},
           // {"sTitle": 'IP Address', mData: "deviceIP"},
            {
                "sTitle": 'Action',
                mData: null,
                bSortable: false,
                mRender: function(o) {
                    return '<a class="btn btn-xs btn-primary" onclick="editDevcie(' + o.deviceId + ',' + '\'' + o.deviceName + '\'' + ',' + '\'' + o.deviceToken + '\'' + ')"><span class="mobile-hide"><i class="glyphicon glyphicon-pencil"></i>Edit</span></a>&nbsp;&nbsp;'
                        + '<a class="btn btn-xs btn-primary" onclick="listDevices(' + o.userId + ')"><span class="mobile-hide"><i class="glyphicon glyphicon-cog"></i>View Devices</span></a>&nbsp;&nbsp;';
                }
            }
        ],
        "fnInitComplete": function(oSettings) {
            $('.dataTables_filter label').append("<i class='glyphicon glyphicon-search' style='position:relative; right:20px;'></i>");
        }
    });
};

var listDevices = function(id) {
    window.location.assign("devices");
};
