var validate_httpport=function()
{
    var valid=1;
    var k=$("#httpsport").val();
    var c=$("#httpport").val();
     if (c <= 1024 && c != 80 || c > 65535)
    {
        $("#ntwk_httpport").val(80);
         $("#warn-alert").fadeIn(30);
        $("div.content-alert").html("Port number should be in between 1024 and 65536");
        valid = 0;
        return false;
    }
    if (k <= 1024 && k != 81 || k > 65535)
    {
        $("#httpsport").val(81);
        $("#warn-alert").fadeIn(30);
$("div.content-alert").html("Port number should be in between 1024 and 65536");
        
        valid = 0;
        return false;
    }
 
};
var general_apply=function()
{
    var valid=validate_httpport();
    if(valid!=1)
    {
        return false;
    }
};