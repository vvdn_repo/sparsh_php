<?php

namespace Device\model;

use Application\Entity\SparshDevice;
use Application\Model\AbstractOrmManager;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container as SessionContainer;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;

class Device extends AbstractOrmManager implements ServiceLocatorAwareInterface {

    protected $serviceManager;
    protected $orgEntityInstance;

    public function __construct() {
        
    }

    public function getServiceLocator() {
        return $this->serviceManager;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceManager = $serviceLocator;
    }

    public function getRepository() {
        $repository = $this->getObjectManager($this->serviceManager)->getRepository('Application\Entity\SparshDevice');
        return $repository;
    }

    public function getEntityInstance() {
        if (null === $this->deviceEntityInstance) {
            $this->deviceEntityInstance = new SparshDevice();
        }
        return $this->deviceEntityInstance;
    }

    public function validateDevicemac($device_mac) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->where("device.deviceStatus != 2");
        $qb->select($qb->expr()->count('device.deviceId') . ' AS deviceCount')
                ->andWhere("device.deviceMac = :deviceMac")
                ->setParameter("deviceMac", $device_mac);
        $result = $qb->getQuery()->getSingleScalarResult();
        return $result;
    }

    public function validateDevicemacWithUser($device_mac, $user_id) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $selectColumns = $selectColumns = array('device.deviceId');
        $qb->where("device.deviceStatus != 2")
                ->andWhere("device.deviceMac = :deviceMac")
                ->andWhere("device.deviceCreatedbyFk = :userId")
                ->setParameter("deviceMac", $device_mac)
                ->setParameter("userId", $user_id);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }
    public function validateDevicemacWithToken($deviceMAC, $deviceToken){
       $qb = $this->getRepository()->createQueryBuilder("device");
        $selectColumns = $selectColumns = array('device.deviceId','device.deviceToken');
        $qb->select($selectColumns);
        $qb->where("device.deviceStatus != 2")
                ->andWhere("device.deviceMac = :deviceMac")
                ->andWhere("device.deviceToken = :deviceToken")
                ->setParameter("deviceMac", $deviceMAC)
                ->setParameter("deviceToken", $deviceToken);
        //$result = $qb->getQuery()->getArrayResult();
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        //print_r($qb->getQuery());die;
        return $result; 
    }
    public function DevicemacExist($device_mac) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->where("device.deviceStatus != 2");
        $qb->select($qb->expr()->count('device.deviceId') . ' AS deviceCount')
                ->andWhere("device.deviceMac = :deviceMac")                
                ->setParameter("deviceMac", $device_mac);
        $result = $qb->getQuery()->getSingleScalarResult();
        return $result;
    }

    public function validateDeviceName($device_name, $user_id) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->where("device.deviceStatus != 2");
        $qb->select($qb->expr()->count('device.deviceId') . ' AS deviceCount')
                ->andWhere("device.deviceName = :deviceName")
                ->andWhere("device.deviceCreatedbyFk = :user_id")
                ->setParameter("deviceName", $device_name)
                ->setParameter("user_id", $user_id);
        $result = $qb->getQuery()->getSingleScalarResult();
        return $result;
    }

    public function validateDevice($user_id, $dlist) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        //$selectColumns = $selectColumns = array('device.deviceId', 'device.deviceName', 'device.deviceType', 'device.deviceMac', 'device.deviceSlnumber', 'device.deviceStatus', 'device.deviceToken', 'device.deviceIP', 'device.deviceFW', 'device.deviceDefault');
        //$qb->select($selectColumns);
        $qb->where("device.deviceStatus != 2");

        $qb->andWhere("device.deviceId IN (:dList)")
                ->andWhere("device.deviceCreatedbyFk = :user_id")
                ->setParameter("dList", $dlist)
                ->setParameter("user_id", $user_id);

        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        //  print_r($result);die;
        return $result;
    }

    public function getActiveDeviceCount($user_id) {

        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->select($qb->expr()->count('device.deviceId') . ' AS deviceCount')
                ->andWhere("device.deviceCreatedbyFk = :userId")
                ->andWhere("device.deviceStatus != 2")
                ->setParameter("userId", $user_id);
        $result = $qb->getQuery()->getSingleScalarResult();
        return $result;
    }

    public function saveDevice(SparshDevice &$device) {
        $om = $this->getObjectManager($this->serviceManager);
        $om->persist($device);
        $om->flush();
        return $device->getDeviceId();
    }

    public function getDeviceList($user_id, $deviceList = array()) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $selectColumns = $selectColumns = array('device.deviceId', 'device.deviceName', 'device.deviceType', 'device.deviceMac', 'device.deviceSlnumber', 'device.deviceStatus', 'device.deviceToken', 'device.deviceIP', 'device.deviceFW', 'device.deviceDefault');
        $qb->select($selectColumns);
        $qb->where("device.deviceStatus != 2")
                ->andWhere("device.deviceCreatedbyFk = :user_id")
                ->setParameter("user_id", $user_id)
                ->orderBy('device.deviceId', 'DESC');
        if (sizeof($deviceList) > 0) {
            $qb->andWhere("device.deviceId IN (:deviceList)")
                    ->setParameter("deviceList", $deviceList)
                    ->orderBy('device.deviceId', 'DESC');
        }
        $result = $qb->getQuery()->getArrayResult();
        //print_r($result);die;
        return $result;
    }

    public function getGroupDevice($user_id, $groupList = array()) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        //$selectColumns = $selectColumns = array('device.deviceId', 'device.deviceName', 'device.deviceType', 'device.deviceMac', 'device.deviceSlnumber', 'device.deviceStatus', 'device.deviceToken', 'device.deviceIP', 'device.deviceFW', 'device.deviceDefault');
        //$qb->select($selectColumns);
        $qb->where("device.deviceStatus != 2");

        $qb->andWhere("device.deviceGroupId IN (:groupList)")
                ->setParameter("groupList", $groupList)
                ->orderBy('device.deviceGroupId', 'DESC');

        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        //  print_r($result);die;
        return $result;
    }

    public function getDeviceNotGroup($user_id, $groupList = array()) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        //$selectColumns = $selectColumns = array('device.deviceId', 'device.deviceName', 'device.deviceType', 'device.deviceMac', 'device.deviceSlnumber', 'device.deviceStatus', 'device.deviceToken', 'device.deviceIP', 'device.deviceFW', 'device.deviceDefault');
        //$qb->select($selectColumns);
        $qb->where("device.deviceStatus != 2");
        $qb->andWhere("device.deviceCreatedbyFk = :userId")
                ->andWhere("device.deviceGroupId NOT IN (:groupList) OR device.deviceGroupId is NULL")
                ->setParameter("groupList", $groupList)
                ->setParameter("userId", $user_id)
                ->orderBy('device.deviceId', 'DESC');

        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        // print_r($result);die;

        return $result;
    }

    public function defaultDevice($user_id, $device_id) {

        $qb = $this->getRepository()->createQueryBuilder("device");
        $query = $qb->update()
                ->set("device.deviceDefault", "?1")
                ->where("device.deviceCreatedbyFk = ?2")
                ->setParameter(1, 0)
                ->setParameter(2, $user_id)
                ->getQuery();
        $query->execute();

        $qb = $this->getRepository()->createQueryBuilder("device");
        $query = $qb->update()
                ->set("device.deviceDefault", "?1")
                ->where("device.deviceId = ?2")
                ->setParameter(1, 1)
                ->setParameter(2, $device_id)
                ->getQuery();
        $query->execute();
        return true;
    }

    public function Device_info($field, $value) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->where("device.$field = :value")
                ->setParameter("value", $value);
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        return $result;
    }

    public function editDevice($data) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $query = $qb->update()
                ->set("device.deviceName", "?1")
                ->where("device.deviceId = ?2")
                ->setParameter(1, $data['device_name'])
                ->setParameter(2, $data['device_id'])
                ->getQuery();
        $query->execute();
        return true;
    }

    public function updateIP($data) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $query = $qb->update()
                ->set("device.deviceIP", "?1")
                ->where("device.deviceMac = ?2")
                ->setParameter(1, $data['device_ip'])
                ->setParameter(2, $data['device_mac'])
                ->getQuery();
        $query->execute();
        return true;
    }

    public function Deviceinfo($mac) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->where("device.deviceMac = :deviceMac")
                ->setParameter("deviceMac", $mac);
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        return $result;
    }

    public function deleteDevice($deviceId) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $query = $qb->update()
                ->set("device.deviceMac", "?1")
                ->set("device.deviceIP", "?4")
                ->set("device.deviceStatus", "?2")
                ->where("device.deviceId = ?3")
                ->setParameter(1, $deviceId)
                ->setParameter(2, "2")
                ->setParameter(3, $deviceId)
                ->setParameter(4, $deviceId)
                ->getQuery();
        $query->execute();
        return true;
    }

    public function setGroup($gid, $data) {
        $deviceids = array();
        $i = 0;
        $qb = $this->getRepository()->createQueryBuilder("device");
        $count = sizeof($data);
        foreach ($data as $d) {
//            ++$i;
//            if ($i == 1) {
//                $deviceids .= "\"";
//            }
            foreach ($d as $key => $value) {
                // $deviceids .= "$value";
                array_push($deviceids, $value);
            }
//            if ($i != $count) {
//                $deviceids .= ",";
//            } else {
//                $deviceids .= "\"";
//            }
        }

        //$deviceids= "1,2";
        $query = $qb->update()
                ->set("device.deviceGroupId", '?1')
                ->where("device.deviceId IN  (:deviceList)")
                ->setParameter(1, $gid)
                ->setParameter(deviceList, $deviceids)
                ->getQuery();
        // print_r($query);die;

        $query->execute();

        return true;
    }
    
  public function removeFromGroup( $data){
      $deviceids = array();
        $i = 0;
        $qb = $this->getRepository()->createQueryBuilder("device");
        $count = sizeof($data);
        foreach ($data as $d) {

            foreach ($d as $key => $value) {
                // $deviceids .= "$value";
                array_push($deviceids, $value);
            }

        }

        $query = $qb->update()
                ->set("device.deviceGroupId", '?1')
                ->where("device.deviceId IN  (:deviceList)")
                ->setParameter(1, NULL)
                ->setParameter(deviceList, $deviceids)
                ->getQuery();
       

        $query->execute();

        return true;
  }

    public function getDeviceConfig($device_id) {

        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->select('device.deviceConfig');
        $qb->where("device.deviceStatus != 2");
        $qb->andWhere("device.deviceId =:deviceId")
                ->setParameter("deviceId", $device_id);

        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        //print_r($result[0]);die;

        return $result;
    }

    public function setDeviceConfig($device_id, $config) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $query = $qb->update()
                ->set("device.deviceConfig", "?1")
                ->where("device.deviceId = ?2")
                ->setParameter(1, $config)
                ->setParameter(2, $device_id)
                ->getQuery();
        $query->execute();
        return true;
    }

    public function getDeviceWifiSettings($device_id) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->select('device.deviceWifiSettings');
        $qb->where("device.deviceStatus != 2");
        $qb->andWhere("device.deviceId =:deviceId")
                ->setParameter("deviceId", $device_id);

        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        //print_r($result[0]);die;

        return $result;
    }

    public function setDeviceWifiSettings($device_id, $device_wifi_settings) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $query = $qb->update()
                ->set("device.deviceWifiSettings", "?1")
                ->where("device.deviceId = ?2")
                ->setParameter(1, $device_wifi_settings)
                ->setParameter(2, $device_id)
                ->getQuery();
        $query->execute();
        return true;
    }
    public function getDeviceEthernetSettings($device_id) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->select('device.deviceEthernetSettings');
        $qb->where("device.deviceStatus != 2");
        $qb->andWhere("device.deviceId =:deviceId")
                ->setParameter("deviceId", $device_id);

        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        //print_r($result[0]);die;

        return $result;
    }

    public function setDeviceEthernetSettings($device_id, $device_ethernet_settings) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $query = $qb->update()
                ->set("device.deviceEthernetSettings", "?1")
                ->where("device.deviceId = ?2")
                ->setParameter(1, $device_ethernet_settings)
                ->setParameter(2, $device_id)
                ->getQuery();
        $query->execute();
        return true;
    }

    public function setViewerMode($device_id, $viewer_mode) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $query = $qb->update()
                ->set("device.deviceViewerAppSetting", "?1")
                ->where("device.deviceId = ?2")
                ->setParameter(1, $viewer_mode)
                ->setParameter(2, $device_id)
                ->getQuery();
        $query->execute();
        return true;
    }

    public function getViewerMode($device_id) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->select('device.deviceViewerAppSetting');
        $qb->where("device.deviceStatus != 2");
        $qb->andWhere("device.deviceId =:deviceId")
                ->setParameter("deviceId", $device_id);

        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        //print_r($result[0]);die;

        return $result;
    }
    public function setLiveMode($device_id, $live_mode) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $query = $qb->update()
                ->set("device.deviceLiveVideoCloud", "?1")
                ->where("device.deviceId = ?2")
                ->setParameter(1, $live_mode)
                ->setParameter(2, $device_id)
                ->getQuery();
        $query->execute();
        return true;
    }

    public function getLiveMode($device_id) {
        $qb = $this->getRepository()->createQueryBuilder("device");
        $qb->select('device.deviceLiveVideoCloud');
        $qb->where("device.deviceStatus != 2");
        $qb->andWhere("device.deviceId =:deviceId")
                ->setParameter("deviceId", $device_id);

        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        //print_r($result[0]);die;

        return $result;
    }


}
