<?php

/**
 * module.config.php handles the routes and paths for the user module 
 * @package Application
 * @author VVDN Technologies < >
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'User\Controller\User' => 'User\Controller\UserController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'register' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/register',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'register',
                    ),
                ),
            ),
            'login' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/login',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'login',
                    ),
                ),
            ),
            'createAdmin' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/createAdmin',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'createAdmin',
                    ),
                ),
            ),
            'adminHome' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/adminHome',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'adminHome',
                    ),
                ),
            ),
            'allUsers' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/allUsers',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'allUsers',
                    ),
                ),
            ),
            'getUsers' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/getusers',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'getUsers',
                    ),
                ),
            ),
            'listDevices' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/user/listdevices[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'listDevices',
                    ),
                ),
            ),
            'getDevice' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/user/getdevices[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'getDevices',
                    ),
                ),
                ),
            'getprofile' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/user/getprofile',
//                    'constraints' => array(
//                        'id' => '[0-9]+',
//                    ),
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'profile',
                    ),
                ),
                ),

            'logout' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/logout',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'logout',
                    ),
                ),
            ),

             'account-success' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/account-success',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'accountSuccess',
                    ),
                ),
            ),

            'generatecaptcha' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/generatecaptcha',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'generatecaptcha',
                    ),
                ),
            ),
            'checkuseremail' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/checkuseremail',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'checkuseremail',
                    ),
                ),
            ),
            'forgotpassword' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/forgotpassword',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'forgotpassword',
                    ),
                ),
            ),
            'resetpassword' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/resetpassword',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'resetpassword',
                    ),
                ),
            ),
            'resetpwd' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/resetpwd',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'resetpwd',
                    ),
                ),
            ),
            'device' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/devices',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'device',
                    ),
                ),
            ),
            
            'viewprofile' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user/viewprofile',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'viewProfile',
                    ),
                ),
            ),
            'activate-account' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user/activate-account/:id',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'activateAccount',
                    ),
                ),
            ),
            'verify-email' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user/verify-email',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'verifyEmail',
                    ),
                ),
            ),


        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'layout/register' => __DIR__ . '/../view/layout/register_login.phtml',
        ),
        'template_path_stack' => array(
            'user' => __DIR__ . '/../view',
        ),
    ),
    'email_whitelist' => array(
        'domains' => array(
        ),
        'patterns' => array(
            'mclouddev#range#@mailinator.com' => array(
                'startLimit' => 1,
                'endLimit' => 200
            ),
            'mcloudtest#range#@mailinator.com' => array(
                'startLimit' => 1,
                'endLimit' => 200
            )
        ),
        'emailids' => array(
        )
    ),
    'email_blacklist' => array(
        'domains' => array(
        ),
        'emailids' => array(
        )
    ),
    'email_validate_check' => array(
        'whitelist' => true,
        'blacklist' => true
    ),
);
