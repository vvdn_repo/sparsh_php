<?php

/**
 * User Registration Form
 * @package User
 * @author VVDN Technologies < >
 */

namespace User\Form;

use Zend\Form\Form;

class UserRegisterForm extends Form {

    public function __construct() {
        parent::__construct('user_register');

        $this->add(array(
            'name' => 'user_email',
            'type' => 'Text',
            'options' => array(
                'label' => 'Email Address',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Email ID'
            ),
        ));
        $this->add(array(
            'name' => 'user_name',
            'type' => 'Text',
            'options' => array(
                'label' => 'Full Name',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Full Name'
            )
        ));
        $this->add(array(
            'name' => 'user_pwd',
            'type' => 'Password',
            'options' => array(
                'label' => 'Password',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Password'
            )
        ));

        $this->add(array(
            'name' => 'user_confirmpwd',
            'type' => 'Password',
            'options' => array(
                'label' => 'Confirm Password',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Confirm Placeholder'
            )
        ));

        $this->add(array(
            'name' => 'user_mobile',
            'type' => 'Text',
            'options' => array(
                'label' => 'Mobile Number',
            ),
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Mobile Number'
            )
        ));
        $this->add(array(
            'name' => 'agree_terms',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'label' => 'I agree to the Terms of Service and Privacy Policy',
                'use_hidden_element' => true
            ),
            'attributes' => array(
                'class' => 'form-control'
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Sign Up',
                'id' => 'submit_register'
            ),
        ));
    }

}
