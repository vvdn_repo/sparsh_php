<?php
/**
 * AuthStorage handles session for logged in user 
 * for the Application module  
 * @package Application
 * @author VVDN Technologies < >
 */

namespace User\Model;

use Zend\Authentication\Storage;

class AuthStorage extends Storage\Session {
    
    /*
     * This function is used to set session timeout for user
     */
    public function setRememberMe($rememberMe = 0, $time = 1209600) {
        if ($rememberMe == 1) {
            $this->session->getManager()->rememberMe($time);
        }
    }

    /*
     * This function is used for forget me functionality
     */
    public function forgetMe() {
        $this->session->getManager()->forgetMe();
    }

}
