<?php

namespace Device\model;

use Application\Entity\SparshGroup;
use Application\Model\AbstractOrmManager;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container as SessionContainer;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;

class Group extends AbstractOrmManager implements ServiceLocatorAwareInterface {

    protected $serviceManager;
    protected $orgEntityInstance;

    public function __construct() {
        
    }

    public function getServiceLocator() {
        return $this->serviceManager;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceManager = $serviceLocator;
    }

    public function getRepository() {
        $repository = $this->getObjectManager($this->serviceManager)->getRepository('Application\Entity\SparshGroup');
        return $repository;
    }

    public function getEntityInstance() {
        if (null === $this->deviceEntityInstance) {
            $this->deviceEntityInstance = new SparshGroup();
        }
        return $this->deviceEntityInstance;
    }

    public function saveGroup(SparshGroup &$group) {
        $om = $this->getObjectManager($this->serviceManager);
        $om->persist($group);
        $om->flush();
        return $group->getGroupId();
    }

    public function deleteGroup($groupId) {
        $qb = $this->getRepository()->createQueryBuilder("group1");
        $query = $qb->update()
                ->set("group1.groupName", "?1")
                ->set("group1.groupStatus", "?2")
                ->where("group1.groupId = ?3")
                ->setParameter(1, $groupId)
                ->setParameter(2, "2")
                ->setParameter(3, $groupId)
                ->getQuery();
        $query->execute();
        return true;
    }

    public function getGroupList($user_id) {
        // print_r($user_id);
        //print_r($result);die;

        $qb = $this->getRepository()->createQueryBuilder("group1");
        $selectColumns = array('group1.groupId', 'group1.groupName');
        $qb->select($selectColumns);
        $qb->where("group1.groupCreatedbyFk = :user_id")
                ->andWhere("group1.groupStatus != 2")
                //->andWhere("device.deviceCreatedbyFk = :user_id")
                ->setParameter("user_id", $user_id);

        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
//print_r($result);die;
        return $result;
    }

    public function getGroup($user_id, $groupId) {
        // print_r($user_id);
        //print_r($result);die;

        $qb = $this->getRepository()->createQueryBuilder("group1");
        $selectColumns = array('group1.groupId');
        $qb->select($selectColumns);
        $qb->where("group1.groupCreatedbyFk = :user_id")
                ->andWhere("group1.groupId = ?1")
                ->andWhere("group1.groupStatus != 2")
                ->setParameter("user_id", $user_id)
                // $qb->where("group.groupCreatedbyFk = :user_id")
                ->setParameter(1, $groupId);
        //  ->orderBy('group.groupId', 'DESC');
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        //$result = $qb->getQuery()->getArrayResult();
        // print_r($user_id);
        //print_r($result);die;
        return $result;
    }

    public function getGroupConfig($group_id) {

        $qb = $this->getRepository()->createQueryBuilder("group1");
        $qb->select('group1.groupConfig');
        $qb->where("group1.groupStatus != 2");
        $qb->andWhere("group1.groupId =:groupId")
                ->setParameter("groupId", $group_id);

        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        //print_r($result[0]);die;

        return $result;
    }

    public function setGroupConfig($group_id, $config) {
        $qb = $this->getRepository()->createQueryBuilder("group1");
        $query = $qb->update()
                ->set("group1.groupConfig", "?1")
                ->where("group1.groupId = ?2")
                ->setParameter(1, $config)
                ->setParameter(2, $group_id)
                ->getQuery();
        $query->execute();
        return true;
    }

    public function validateGroup($user_id, $glist) {
        $qb = $this->getRepository()->createQueryBuilder("group1");
        //$selectColumns = $selectColumns = array('device.deviceId', 'device.deviceName', 'device.deviceType', 'device.deviceMac', 'device.deviceSlnumber', 'device.deviceStatus', 'device.deviceToken', 'device.deviceIP', 'device.deviceFW', 'device.deviceDefault');
        //$qb->select($selectColumns);
        $qb->where("group1.groupStatus != 2");

        $qb->andWhere("group1.groupId IN (:gList)")
                ->andWhere("group1.groupCreatedbyFk = :user_id")
                ->setParameter("gList", $glist)
                ->setParameter("user_id", $user_id);

        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        //  print_r($result);die;
        return $result;
    }

    public function getGroupWifiSettings($group_id) {

        $qb = $this->getRepository()->createQueryBuilder("group1");
        $qb->select('group1.groupWifiSettings');
        $qb->where("group1.groupStatus != 2");
        $qb->andWhere("group1.groupId =:groupId")
                ->setParameter("groupId", $group_id);

        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        //print_r($result[0]);die;

        return $result;
    }

    public function setGroupWifiSettings($group_id, $group_wifi_settings) {
        $qb = $this->getRepository()->createQueryBuilder("group1");
        $query = $qb->update()
                ->set("group1.groupWifiSettings", "?1")
                ->where("group1.groupId = ?2")
                ->setParameter(1, $group_wifi_settings)
                ->setParameter(2, $group_id)
                ->getQuery();
        $query->execute();
        return true;
    }

    public function getViewerMode($group_id) {
        $qb = $this->getRepository()->createQueryBuilder("group1");
        $qb->select('group1.groupViewerAppSetting');
        $qb->where("group1.groupStatus != 2");
        $qb->andWhere("group1.groupId =:groupId")
                ->setParameter("groupId", $group_id);

        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        //print_r($result[0]);die;

        return $result;
    }

    public function setViewerMode($group_id, $viewer_mode) {
        $qb = $this->getRepository()->createQueryBuilder("group1");
        $query = $qb->update()
                ->set("group1.groupViewerAppSetting", "?1")
                ->where("group1.groupId = ?2")
                ->setParameter(1, $viewer_mode)
                ->setParameter(2, $group_id)
                ->getQuery();
        $query->execute();
        return true;
    }
    public function getLiveMode($group_id) {
        $qb = $this->getRepository()->createQueryBuilder("group1");
        $qb->select('group1.groupLiveVideoCloud');
        $qb->where("group1.groupStatus != 2");
        $qb->andWhere("group1.groupId =:groupId")
                ->setParameter("groupId", $group_id);

        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
        //print_r($result[0]);die;

        return $result;
    }

    public function setLiveMode($group_id, $live_mode) {
        $qb = $this->getRepository()->createQueryBuilder("group1");
        $query = $qb->update()
                ->set("group1.groupLiveVideoCloud", "?1")
                ->where("group1.groupId = ?2")
                ->setParameter(1, $live_mode)
                ->setParameter(2, $group_id)
                ->getQuery();
        $query->execute();
        return true;
    }
    
    public function editName($group_id,$group_name){
        $qb = $this->getRepository()->createQueryBuilder("group1");
        $query = $qb->update()
                ->set("group1.groupName", "?1")
                ->where("group1.groupId = ?2")
                ->setParameter(1, $group_name)
                ->setParameter(2, $group_id)
                ->getQuery();
        $query->execute();
        return true;
    }

}
