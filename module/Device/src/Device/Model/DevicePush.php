<?php


namespace Device\Model;

use Application\Entity\SparshDevicePush;
use Application\Model\AbstractOrmManager;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMapping;
use Zend\Config\Reader\Ini;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NoResultException;

use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;
use Zend\Math\Rand;

class DevicePush extends AbstractOrmManager implements ServiceLocatorAwareInterface
{

    public $serviceManager;


    public function getServiceLocator()
    {
        return $this->serviceManager;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceManager = $serviceLocator;
    }

    public function getRepository()
    {
        $repository = $this->getObjectManager($this->serviceManager)->getRepository('Application\Entity\SparshDevicePush');
        return $repository;
    }

    public function getEntityInstance() {
        if (null === $this->configEntityInstance) {
            $this->configEntityInstance = new SparshDevicePush();
        }
        return $this->configEntityInstance;
    }



    public function saveDevicePush(SparshDevicePush &$devicepush) {

        $om = $this->getObjectManager($this->serviceManager);
        $om->persist($devicepush);
        $om->flush();
	$om->clear();
        return $devicepush->getDevicePushId();
    }



    public function InsertDevicePush($pushCommand,$param,$device_id,$user_id){
        //print_r("heeer");
        //print_r(func_get_args());die;

        $devicepushEntity = $this->getEntityInstance();
        $objectManager = $this->getObjectManager($this->getServiceLocator());
        $deviceProxyInstance = $objectManager->getReference('Application\Entity\SparshDevice', $device_id);
        $devicepushEntity->setDeviceIdFK($deviceProxyInstance);
        $userProxyInstance = $objectManager->getReference('Application\Entity\SparshUser', $user_id);
        $devicepushEntity->setDevicePushUserIdFk($userProxyInstance);
        $modelCommon = $this->getServiceLocator()->get('Common');
        $time = $modelCommon->getCurrentTimeStamp();
	$time = $time;
	$devicepushEntity->setDevicePushCommand(htmlspecialchars($pushCommand));
        $devicepushEntity->setTimeStamp(htmlspecialchars($time));
        $devicepushEntity->setDevicePushStatus('pending');
        $devicepushEntity->setDevicePushParam($param);
        $devicepushId = $this->saveDevicePush($devicepushEntity);
	return $devicepushId;
    }

     

     public function updateDevicePush($devicePush_id) {
        $qb = $this->getRepository()->createQueryBuilder("devicepush");
        $query = $qb->update()
            ->set("devicepush.devicepushStatus", "?1")
            ->where("devicepush.devicepushId = ?2")
            ->setParameter(1, "completed")
            ->setParameter(2, $devicePush_id)
            ->getQuery();
        $query->execute();
        return true;

    }

	public function updateDevicePush1($user_id) {
        $qb = $this->getRepository()->createQueryBuilder("devicepush");
        $query = $qb->update()
            ->set("devicepush.devicepushStatus", "?1")
            ->set("devicepush.stopAlarmStatus", "?3")
            ->where("devicepush.userIdFk = ?2")
            ->setParameter(1, "completed")
            ->setParameter(2, $user_id)
            ->setParameter(3, 0)
            ->getQuery();
        $query->execute();
        return true;

    }

   
   public function Device_push() {
        $modelCommon = $this->getServiceLocator()->get('Common');
        $time = $modelCommon->getCurrentTimeStamp();
        $qb = $this->getRepository()->createQueryBuilder("devicepush");
        $qb->where("devicepush.devicepushStatus = :status")
            ->andWhere("devicepush.devicepushTimeStamp < :time")
            ->setParameter("status","pending")
            ->setParameter("time", $time);
        $result = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();

        return $result;

    }

  public function Device_push_delete() {
        $modelCommon = $this->getServiceLocator()->get('Common');
        $time = $modelCommon->getCurrentTimeStamp();
        $qb = $this->getRepository()->createQueryBuilder("devicepush");
        $query = $qb->update()
            ->set("devicepush.devicepushStatus", "?1")
            ->where("devicepush.devicepushTimeStamp +10 < :time")
            ->setParameter(1, "completed")
            ->setParameter("time", $time)
            ->getQuery();
        $query->execute();
        return true;

    }

	




}