<?php

/**
 * OrganizationController handles Organization related views
 * @package Organization
 * @author VVDNTechnologies < >
 */

namespace Device\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\Config\Reader\Ini;
use Zend\Config\Reader\Json;
use Zend\Session\Container;
use Zend\Math\Rand;
use Zend\Http\Client;
use Zend\Serializer\Serializer;
use Zend\Http\Response;

class DeviceController extends AbstractActionController {

    protected $sessionStorage;
    protected $authService;

    /*
     * This function will return ZF2 service instance
     * @param String
     * @return Object of ServiceLocatorInterface
     */

    protected function getServiceInstance($service) {
        return $this->getServiceLocator()->get($service);
    }

    public function registerAction() {
        $response = array();
        $request = $this->getRequest();
        $this->authService = $this->getServiceInstance('AuthService');

        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $formData = $data;
            //print_r($data);

            $validateModel = $this->getServiceInstance('Validate');
            $valid_mac = $validateModel->macValidate($data['device_mac']);
            $data['user_token'] = $validateModel->stripSpace($data['user_token']);
            $data['user_pin'] = $validateModel->stripSpace($data['user_pin']);
            if ($valid_mac == 1) {
                $deviceModel = $this->getServiceInstance('Device');
                $userModel = $this->getServiceInstance('User');
                $isUnique = $deviceModel->validateDevicemac($data['device_mac']);
                $user_id = $userModel->getUidfromtoken($data['user_token']);
                $match = $userModel->getMatch($data['user_token'], $data['user_pin']);
                //$admin = $userModel->getRole($data['user_token']);
                // print_r($match);die;
                //$admin ==1;
                if ($user_id) {
                    if ($match[0][userPin] == $data['user_pin']) {
                        $config = parse_ini_file('config/config.ini');
                        $proxy_ip = $config['proxy_ip'];
                        $proxy_port = $config['proxy_port'];
                        if ($isUnique == 0) {

                            // BEGIN-Create Device
                            $deviceEntity = $deviceModel->getEntityInstance();
                            $objectManager = $deviceModel->getObjectManager($this->getServiceLocator());
                            $created_time = time();
                            // $deviceEntity->setDeviceName(htmlspecialchars($data['device_name']));
                            $deviceEntity->setDeviceType(htmlspecialchars($data['device_type']));
                            $deviceEntity->setDeviceMac(htmlspecialchars($data['device_mac']));
                            $deviceEntity->setDeviceName(htmlspecialchars($data['device_name']));
                            $deviceEntity->setDeviceCreatedon($created_time);
                            $deviceEntity->setDeviceIP(htmlspecialchars($data['device_ip']));
                            $userProxyInstance = $objectManager->getReference('Application\Entity\SparshUser', $user_id);
                            $deviceEntity->setDeviceCreatedbyFk($userProxyInstance);
                            $deviceEntity->setDeviceStatus(0); //0-> InActive Account
                            $deviceEntity->setDeviceDefault(0);
                            $deviceEntity->setDeviceViewerAppSetting(0);
                            $deviceEntity->setDeviceLiveVideoCLoud(0);
                            $deviceId = $deviceModel->saveDevice($deviceEntity);

                            $rand_num = Rand::getString(5, 'abcdefghijklmnopqrstuvwxyz0123456789', true);
                            $device_token = "s_" . $deviceId . "_" . $rand_num;
                            $deviceEntity->setDeviceToken($device_token);
                            $device = $deviceModel->saveDevice($deviceEntity);
                            //print_r($device);
                            if ($device) {

                                $result = [0];
                                $device_id = $device;
                                $device_token = $device_token;
                            }
                            $result = "true#" . $device_id . "#" . $device_token . "#" . $proxy_ip . "#" . $proxy_port;
                          print_r($result);
                            return true;
                            //return new JsonModel(array("result" => $result, "proxy_ip" => $proxy_ip, "proxy_port" => $proxy_port, "device_id" => $device_id, "device_token" => $device_token));
                        } else {

                            $isUniqueWithUser = $deviceModel->validateDevicemacWithUser($data['device_mac'], $user_id);
                            // print_r($isUniqueWithUser) ;die;
                            if ($isUniqueWithUser) {
                                // echo 'here'; die;
                                $deviceId = $isUniqueWithUser[0]['deviceId'];
                                $DeviceModel = $this->getServiceInstance('Device');
                                $delete = $DeviceModel->deleteDevice($deviceId);

                                // BEGIN-Create Device
                                $deviceEntity = $deviceModel->getEntityInstance();
                                $objectManager = $deviceModel->getObjectManager($this->getServiceLocator());
                                $created_time = time();
                                // $deviceEntity->setDeviceName(htmlspecialchars($data['device_name']));
                                $deviceEntity->setDeviceType(htmlspecialchars($data['device_type']));
                                $deviceEntity->setDeviceMac(htmlspecialchars($data['device_mac']));
                                $deviceEntity->setDeviceName(htmlspecialchars($data['device_name']));
                                $deviceEntity->setDeviceCreatedon($created_time);
                                $deviceEntity->setDeviceIP(htmlspecialchars($data['device_ip']));
                                $userProxyInstance = $objectManager->getReference('Application\Entity\SparshUser', $user_id);
                                $deviceEntity->setDeviceCreatedbyFk($userProxyInstance);
                                $deviceEntity->setDeviceStatus(0); //0-> InActive Account
                                $deviceEntity->setDeviceDefault(0);
                                $deviceEntity->setDeviceViewerAppSetting(0);
                                $deviceEntity->setDeviceLiveVideoCLoud(0);

                                $deviceId = $deviceModel->saveDevice($deviceEntity);

                                $rand_num = Rand::getString(5, 'abcdefghijklmnopqrstuvwxyz0123456789', true);
                                $device_token = "s_" . $deviceId . "_" . $rand_num;
                                $deviceEntity->setDeviceToken($device_token);
                                $device = $deviceModel->saveDevice($deviceEntity);
                                //print_r($device);die;
                                if ($device) {
                                    //$PushNotificationModel = $this->getServiceLocator()->get('PushNotification');
                                    // $msg = "You have a new voice mail"."_".$device_info[0]['deviceIP'];
                                    // $PushNotificationModel->makePushNotification($user_id,$msg);
                                    /* $status = true;
                                      $message = "Device Added Successfully"; */
                                    $result = [0];
                                    $device_id = $device;
                                    $device_token = $device_token;
                                }
                                $result = "true#" . $device_id . "#" . $device_token . "#" . $proxy_ip . "#" . $proxy_port;
                                print_r($result);
                                return true;
                                //return new JsonModel(array("result" => $result, "proxy_ip" => $proxy_ip, "proxy_port" => $proxy_port, "device_id" => $device_id, "device_token" => $device_token));
                            } else {
                                $error = 1;
                                $message = "MAC Already Used";
                            }
                        }
                    } else {
                        $error = 2;
                        $message = "Token - pin match error";
                    }
                } else {
                    $error = 3;
                    $message = "No user found for given user_token";
                }
                //}
            } else {
                $error = 4;
                $message = "Device MAC not Valid";
            }
        } else {
            $error = 5;
            $message = "Failed";
        }
        print_r("false");
        return true;
        //return new JsonModel(array("error" => [$error, "$message"]));
    }

    public function CheckDeviceMacAction() {
        $response = array();
        $request = $this->getRequest();
        $this->authService = $this->getServiceInstance('AuthService');

        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $formData = $data;
            $mac = $data['device_mac'];
            $DeviceModel = $this->getServiceInstance('Device');
            $isRegistered = $DeviceModel->DevicemacExist($mac);
            //print_r($isRegistered);die;
            if ($isRegistered == 1) {
                $status = true;
            } else {
                $status = false;
            }
        }


        return new JsonModel(array("status" => $status));
    }

    public function checkNameAction() {
        $session = new Container('base');
        $user_id = $session->offsetGet('adminuser_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = json_decode(file_get_contents("php://input"), true);
                $DeviceModel = $this->getServiceInstance('Device');
                $isUniqueName = $DeviceModel->validateDeviceName($data['device_name'], $user_id);
                if ($isUniqueName == 0) {
                    $result = [0];
                    //$message = "Name available";
                } else {
                    $error = 10;
                    $message = "Name already used";
                }
            } else {
                $error = 7;
                $message = "Fail";
            }
        } else {
            $error = 1;
            $message = "Your Session Expired ";
        }

        return new JsonModel(array("result" => $result, "error" => [$error, "$message"]));
    }

    public function deviceIdentityAction() {
        $response = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $commandId = $data['commandId'];
            $deviceMAC = $data['device_mac'];
            $deviceToken = $data['device_token'];
            //print_r($data);
            if (!empty($deviceMAC) and ! empty($deviceToken)) {
                $deviceModel = $this->getServiceInstance('Device');
                $deviceEntity = $deviceModel->getEntityInstance();
                $objectManager = $deviceModel->getObjectManager($this->getServiceLocator());

                $deviceData = $deviceModel->validateDevicemacWithToken($deviceMAC, $deviceToken);
                //print_r($deviceData);die;
                if ($deviceData) {
                    //echo 'here';print_r($deviceData[0]['deviceToken']);die;
                    $commandId = $data['commandId'];
                    $status = true;
                    $message = "Device Found";
                    $device_token = $deviceData[0]['deviceToken'];
                    $device_id = $deviceData[0]['deviceId'];
                } else {
                    $commandId = $data['commandId'];
                    $status = false;
                    $message = "Device Not Found";
                    $device_token = "";
                    $device_id = "";
                }
            } else {
                $commandId = $data['commandId'];
                $status = false;
                $message = "device_mac And device_token can not be null";
                $device_token = "";
                $device_id = "";
            }
        } else {
            $commandId = "";
            $status = false;
            $message = "Fail";
            $device_token = "";
            $device_id = "";
        }
        return new JsonModel(array("commandId" => $commandId, "connection" => "ok", "status" => $status, "message" => $message, "device_token" => $device_token, "device_id" => $device_id));
    }

    public function deleteAction() {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = json_decode(file_get_contents("php://input"), true);
                $deviceId = $data['device_id'];
                $DeviceModel = $this->getServiceInstance('Device');
                $device_Id = $DeviceModel->getDeviceList($user_id, $deviceId);
                //print_r($device_Id);die;
                if ($device_Id) {
                    $delete = $DeviceModel->deleteDevice($deviceId);
                    if ($delete) {
                        $status = true;
                        $message = "Device Deleted";
                    }
                } else {
                    $status = false;
                    $message = "Device Not Belong to Your Account";
                }
            } else {
                $status = false;
                $message = "Fail";
            }
        } else {
            $status = false;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("status" => $status, "message" => $message));
    }

    public function listAction() {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $this->authService = $this->getServiceInstance('AuthService');
            $data = json_decode(file_get_contents("php://input"), true);
            $formData = $data;
            $deviceModel = $this->getServiceInstance('Device');
            $device_list = $deviceModel->getDeviceList($user_id);
            $config = parse_ini_file('config/config.ini');
            $url = $config['rtsp_url'];

            if ($device_list) {
                $new = array();
                $i = 0;
                foreach ($device_list as $d) {
                    $new[$i][deviceId] = $d[deviceId];
                    if (empty($d[deviceName])) {
                        $new[$i][deviceName] = $d[deviceMac];
                    } else {
                        $new[$i][deviceName] = $d[deviceName];
                    }
                    $new[$i][deviceMac] = $d[deviceMac];
                    $new[$i][deviceToken] = $d[deviceToken];
                    $new[$i][deviceRtspUrl] = $url . '/'.$d[deviceToken];
                    $i++;
                }

                $response['status'] = true;
                $response['data'] = $new;
                return new JsonModel($response);
            } else {
                $response['status'] = true;
                $response['data'] = [];
                $response = $this->getResponseWithHeader()
                        ->setContent(json_encode($response));
                return $response;
            }
//            } else {
//                $error = 2;
//                $message = "Admin can view device list";
//            }
        } else {
            $error = 1;
            $message = "Your Session Expired";
        }

        return new JsonModel(array("error" => [$error, "$message"]));
    }

    public function getResponseWithHeader() {
        $response = $this->getResponse();
        $response->getHeaders()
                //make can accessed by *
                ->addHeaderLine('Access-Control-Allow-Origin', '*')
                //set allow methods
                ->addHeaderLine('Access-Control-Allow-Methods', 'POST PUT DELETE GET');
        return $response;
    }

    public function editDeviceAction() {
        $session = new Container('base');
        $user_id = $session->offsetGet('adminuser_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = json_decode(file_get_contents("php://input"), true);
                $deviceId = $data['device_id'];
                $deviceModel = $this->getServiceInstance('Device');
                $deviceData = $deviceModel->Device_info('deviceId', $deviceId);
                if ($deviceData) {
                    $device_Id = $deviceModel->getDeviceList($user_id, $deviceId);
                    if ($device_Id) {
                        $objectManager = $deviceModel->getObjectManager($this->getServiceLocator());
                        $result = $deviceModel->editDevice($data);
                        if ($result) {
                            $status = true;
                            $message = "Device Edited Successfully";
                            return new JsonModel(array("status" => $status, "message" => $message));
                        }
                    } else {
                        $status = false;
                        $message = "Device Not Belong to your Account";
                    }
                } else {
                    $status = false;
                    $message = "No device found for given device_id";
                }
            } else {
                $status = false;
                $message = "Fail";
            }
        } else {
            $status = false;
            $message = "Your Session Expired";
        }
        return new JsonModel(array("status" => $status, "message" => $message));
    }

    public function updateIpAction() {
        $response = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $mac = $data['device_mac'];
            $ip = $data['device_ip'];
            if (!empty($ip) and ! empty($mac)) {
                $deviceModel = $this->getServiceInstance('Device');
                //$deviceEntity = $deviceModel->getEntityInstance();
                //$objectManager = $deviceModel->getObjectManager($this->getServiceLocator());;
                $deviceData = $deviceModel->Deviceinfo($mac);
                if ($deviceData) {
                    $update = $deviceModel->updateIP($data);
                    if ($update) {
                        $status = true;
                        $message = "IP Updated";
                    } else {
                        $status = false;
                        $message = "IP Not Updated";
                    }
                } else {
                    $status = false;
                    $message = "No Device Found for given Device Token";
                }
            } else {
                $status = false;
                $message = "device_mac And device_ip can not be null";
            }
        } else {
            $status = false;
            $message = "Fail";
        }
        return new JsonModel(array("status" => $status, "message" => $message));
    }

}
