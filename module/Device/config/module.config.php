<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Device\Controller\Device' => 'Device\Controller\DeviceController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'device-register' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/register',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'register'
                    )
                ),
            ),
            'check-devicemac' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/check-device',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'CheckDeviceMac'
                    )
                ),
            ),
            'check-name' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/check-name',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'checkName'
                    )
                ),
            ),
            'device-delete' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/delete',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'delete'
                    )
                ),
            ),
            'group-delete' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/delete-group',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'deleteGroup'
                    )
                ),
            ),
            'device-list' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/list',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'list'
                    )
                ),
            ),
            'default-device' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/default',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'defaultCamera'
                    )
                ),
            ),
            'update-ip' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/update-ip',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'updateIp'
                    )
                ),
            ),
            'device-identity' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/get-device-identity',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'deviceIdentity'
                    )
                ),
            ),
            'create-group' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/create-group',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'createGroup'
                    )
                ),
            ),
            'list-group' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/list-group',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'listGroup'
                    )
                ),
            ),
            'update-device-config' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/update-device-config',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'updateDeviceConfig'
                    )
                ),
            ),
            'get-device-config' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/get-device-config',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'getDeviceConfig'
                    )
                ),
            ),
            'update-group-config' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/update-group-config',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'updateGroupConfig'
                    )
                ),
            ),
            'get-group-config' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/get-group-config',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'getGroupConfig'
                    )
                ),
            ),
            'update-device-wifi-settings' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/update-device-wifi-settings',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'updateDeviceWifiSettings'
                    )
                ),
            ),
            'get-device-wifi-settings' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/get-device-wifi-settings',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'getDeviceWifiSettings'
                    )
                ),
            ),
            'update-device-ethernet-settings' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/update-device-ethernet-settings',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'updateDeviceEthernetSettings'
                    )
                ),
            ),
            'get-device-ethernet-settings' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/get-device-ethernet-settings',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'getDeviceEthernetSettings'
                    )
                ),
            ),
            'update-device-viewer-mode' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/update-device-viewer-mode',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'updateDeviceViewerAppSettings'
                    )
                ),
            ),
            'get-device-live-mode' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/get-device-live-mode',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'getDeviceLiveVideo'
                    )
                ),
            ),
            'update-device-live-mode' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/update-device-live-mode',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'updateDeviceLiveVideo'
                    )
                ),
            ),
            'get-device-viewer-mode' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/get-device-viewer-mode',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'getDeviceViewerAppSettings'
                    )
                ),
            ),
            'update-group-viewer-mode' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/update-group-viewer-mode',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'updateGroupViewerAppSettings'
                    )
                ),
            ),
            'get-group-viewer-mode' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/get-group-viewer-mode',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'getGroupViewerAppSettings'
                    )
                ),
            ),
            'update-group-live-mode' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/update-group-live-mode',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'updateGroupLiveVideo'
                    )
                ),
            ),
            'get-group-live-mode' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/get-group-live-mode',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'getGroupLiveVideo'
                    )
                ),
            ),
            'update-group-wifi-settings' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/update-group-wifi-settings',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'updateGroupWifiSettings'
                    )
                ),
            ),
            'get-group-wifi-settings' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/get-group-wifi-settings',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'getGroupWifiSettings'
                    )
                ),
            ),
            'get-resolutionlist' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/get-resolutionlist',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'getResolutionList'
                    )
                ),
            ),
            'start-cronjob' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/device/start-devicepush',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'DevicePush',
                    ),
                ),
            ),
            'edit-group' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/edit-group',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'editGroup'
                    )
                ),
            ),
            'edit-device' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/device/edit-device',
                    'defaults' => array(
                        'controller' => 'Device\Controller\Device',
                        'action' => 'editDevice'
                    )
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    )
);
