<?php

namespace Device;

use Device\Model\Device;
use Device\Model\Group;
//use Device\Model\Cube;
use Device\Model\Command;
use Device\Model\DeviceConfig;
use Device\Model\Sensor;
use Device\Model\DevicePush;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use Zend\Authentication\AuthenticationService;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module {

    public function onBootstrap(MvcEvent $e) {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Device' => function($sm) {
                    $device = new Device();
                    $device->setServiceLocator($sm);
                    return $device;
                },
                'Group' => function($sm) {
                    $group = new Group();
                    $group->setServiceLocator($sm);
                    return $group;
                },
                'DevicePush' => function($sm) {
                    $devicepush = new DevicePush();
                    $devicepush->setServiceLocator($sm);
                    return $devicepush;
                },
            ),
        );
    }

}
