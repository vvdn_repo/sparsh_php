<?php

/**
 * This model is used to handle Validation functions  
 * @package Alarm
 * @author VVDNTechnologies < >
 */

namespace Api\Model;

use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;

class Validate {

    protected $serviceManager;

    public function getServiceLocator() {
        return $this->serviceManager;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceManager = $serviceLocator;
    }

    public function pwdValidate($data) {
        if (preg_match('/^[^\'"{}()]{8,30}$/', $data)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function emailValidate($data) {
        if (filter_var($data, FILTER_VALIDATE_EMAIL)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function alphaNumericValidate($data) {
        //print_r("$data") ;die;
        if (preg_match('/^[\w\d]{1,30}$/', $data)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function NumericValidate($data) {
        if (preg_match('/^[1-9][0-9]*$/', $data)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function macValidate($data) {
        if (preg_match('/^([0-9A-Fa-f]{2}[:-]){5}[0-9A-Fa-f]{2}$/', $data)) {
            return 1;
        } else {
            return 0;
        }
    }

//    public function isAdminValidate($data){
//        if(preg_match('/^[0][1]/', $data))
//        {
//          return 1;
//        } else {
//            return 0;
//        }
//    }
    public function MobValidate($data) {
        if (preg_match('/^[\d]{10,10}$/', $data)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function usernameValidate($data) {
        if (preg_match('/^[A-Za-z][A-Za-z0-9]{0,30}$/', $data)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function stripSpace($data) {
        $data = preg_replace('/\s+/', '', $data);
        return $data;
    }

    public function wifi_passValidate($mode, $passphrase) {
        if ($mode == 0 && empty($passphrase)) {
            return 1;
        } elseif ($mode == 1 && empty($passphrase)) {
            return 0;
        } elseif ($mode == 0 && !empty($passphrase)) {
            return 0;
        } elseif ($mode == 1) {
            if (preg_match('/^[^\']{1,64}$/', $passphrase)) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    public function ssidValidate($ssid) {
        if (preg_match('/^[^\']{1,32}$/', $ssid)) {
            return 1;
        } else {
            return 0;
        }
    }

    //network settings
    public function validateIPfield($ipfield) {
        $str_ipfield = explode(".", $ipfield);
        $str_ipfield1 = $str_ipfield[0];
        $str_ipfield2 = $str_ipfield[1];
        $str_ipfield3 = $str_ipfield[2];
        $str_ipfield4 = $str_ipfield[3];

        $pattern = '/^(2[0-2][0-3]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/';
        $match = preg_match($pattern, $ipfield);

        if (!$match) {
            //alert("You have entered an invalid IP address!"); 
            return 0;
        }
        if ($ipfield == "0.0.0.0") {
            //alert ("The IP Address field cannot be 0.0.0.0");
            return 0;
        }
        if ($ipfield == "255.255.255.255") {
            //alert ("The IP Address field cannot be 255.255.255.255");
            return 0;
        }
        if ($ipfield == "127.0.0.1") {
            //alert ("The IP Address field cannot be 127.0.0.1");
            return 0;
        }
        if ($str_ipfield1 == 0 || $str_ipfield4 == 0) {
            return 0;
        }
        if ($str_ipfield4 == 255) {
            return 0;
        }
        return 1;
    }

    public function validateMask($maskfield) {
        $pattern = '/^(25[0-5]|2[0-4][0-9]|[01]?[1-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[1-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[1-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/';
        $match = preg_match($pattern, $maskfield);

        if (!$match) {
            //alert("You have entered an invalid IP address!"); 
            return 0;
        }
        if ($maskfield == "0.0.0.0") {
            //alert ("The IP Address field cannot be 0.0.0.0");
            return 0;
        }
        return 1;
    }

    public function DecToBin($dec) {
        //print_r($dec);
        $bin = "";
        while ($dec >= 1) {
            $rim = $dec % 2;
            $dec = $dec / 2;
            $bin = $rim + $bin;
        }
        // print_r($bin);die;
        while (strlen($bin) < 8) {
            // print_r($bin);
            $bin = "0" . $bin;
        }
        // print_r($bin);die;
        return $bin;
    }

    public function BitwiseNot($dec) {
        $bin = "";
        $num = 0;
        while ($dec >= 1) {
            $rim = $dec % 2;
            $dec = $dec / 2;
            if ($rim == 0)
                $bin = "1" . $bin;
            else
                $bin = "0" . $bin;
        }
        while (strlen($bin) < 8) {
            $bin = "1" . $bin;
        }
        $l = strlen($bin) - 1;
        for ($i = 0; $i <= $l; $i++) {
            $val_bin = $bin[$l - $i];
            $val = (int) preg_replace('/[^0-9]/', '', $val_bin);;
            $bin_pow = pow(2, i);
            $num = $num + ($val * $bin_pow);
        }
        return $num;
    }

    public function validateSubnetMask($oct1) {
        $str_oct = explode(".", $oct1);
        $str_oct1 = $str_oct[0];
        $str_oct2 = $str_oct[1];
        $str_oct3 = $str_oct[2];
        $str_oct4 = $str_oct[3];
        $oct = $this->DecToBin($str_oct1) + $this->DecToBin($str_oct2) + $this->DecToBin($str_oct3) + $this->DecToBin($str_oct4);
        $l = strlen($oct);
        $flag = 0;
        $valid = 1;
        for ($i = 0; $i <= $l; $i++) {
            if ($flag == 1 && $oct[$i] == 1) {
                $valid = 0;
            }
            if ($flag == 0) {
                if ($oct[$i] == 0)
                    $flag = 1;
            }
        }
        if ($oct1 == "255.255.255.255") {
            $valid = 0;
        }
        return $valid;
    }

    public function checkBroadcast($ioct1, $moct1) {
        $ip_brd = explode(".", $oct1);
        $ip_brd1 = $ip_brd[0];
        $ip_brd2 = $ip_brd[1];
        $ip_brd3 = $ip_brd[2];
        $ip_brd4 = $ip_brd[3];

        $mask_brd = explode(".", $moct1);
        $mask_brd1 = $mask_brd[0];
        $mask_brd2 = $mask_brd[1];
        $mask_brd3 = $mask_brd[2];
        $mask_brd4 = $mask_brd[3];

        $ipoct = $this->DecToBin($ip_brd1) + $this->DecToBin($ip_brd2) + $this->DecToBin($ip_brd3) + $this->DecToBin($ip_brd4);
        $moct = $this->DecToBin($mask_brd1) + $this->DecToBin($mask_brd2) + $this->DecToBin($mask_brd3) + $this->DecToBin($mask_brd4);
        $count = 0;
        $valid = 1;
        $nmask = $this->BitwiseNot($mask_brd1);
        if (($ip_brd1 & $nmask) == $nmask)
            $count++;
        $nmask = $this->BitwiseNot($mask_brd2);
        if (($ip_brd2 & $nmask) == $nmask)
            $count++;
        $nmask = $this->BitwiseNot($mask_brd3);
        if (($ip_brd3 & $nmask) == $nmask)
            $count++;
        $nmask = $this->BitwiseNot($mask_brd4);
        if (($ip_brd4 & $nmask) == $nmask)
            $count++;

        if ($count == 4)
            $valid = 0;

        return $valid;
    }

    public function checkIpCombination($ioct1, $gwoct1, $moct1) {
        $ip_comb = explode(".", $ioct1);
        $ip_comb1 = $ip_comb[0];
        $ip_comb2 = $ip_comb[1];
        $ip_comb3 = $ip_comb[2];
        $ip_comb4 = $ip_comb[3];
        //print_r($ip_comb);

        $mask_comb = explode(".", $moct1);
        $mask_comb1 = $mask_comb[0];
        $mask_comb2 = $mask_comb[1];
        $mask_comb3 = $mask_comb[2];
        $mask_comb4 = $mask_comb[3];
//print_r($mask_comb);
        $gw_comb = explode(".", $gwoct1);
        $gw_comb1 = $gw_comb[0];
        $gw_comb2 = $gw_comb[1];
        $gw_comb3 = $gw_comb[2];
        $gw_comb4 = $gw_comb[3];
//print_r($gw_comb);
        $valid = 0;
        $im1 = $ip_comb1 & $mask_comb1;
        $gm1 = $gw_comb1 & $mask_comb1;
        $im2 = $ip_comb2 & $mask_comb2;
        $gm2 = $gw_comb2 & $mask_comb2;
        $im3 = $ip_comb3 & $mask_comb3;
        $gm3 = $gw_comb3 & $mask_comb3;
        $im4 = $ip_comb4 & $mask_comb4;
        $gm4 = $gw_comb4 & $mask_comb4;
       // print_r($im1.":1:".$gm1);echo "\n";print_r($im2.":2:".$gm2);echo "\n";print_r($im3.":3:".$gm3);echo "\n";print_r($im4.":4:".$gm4);echo "\n";
        if (($im1 == $gm1) && ($im2 == $gm2) && ($im3 == $gm3) && ($im4 == $gm4))
            $valid = 1;
        return $valid;
    }

    public function ValidateAll($ipaddr, $mask, $gw, $dns) {
        $net_ipaddr = $ipaddr;
        $net_mask = $mask;
        $net_gw = $gw;
        $net_dns = $dns;
        //$net_adns = $adns;
        $valid = 1;

        $validip = $this->validateIPfield($net_ipaddr);
        if ($validip == 0) {
            $valid = 0;
            return 0;
        }
        $validmask = $this->validateMask($net_mask);
        if ($validmask == 0) {
            $valid = 0;
            return 0;
        }
        $validgw = $this->validateIPfield($net_gw);
        if ($validgw == 0) {
            $valid = 0;
            return 0;
        }
        $validdns = $this->validateIPfield($net_dns);
        if ($validdns == 0) {
            $valid = 0;
            return 0;
        }
//        $validadns = $this->validateIPfield($net_adns);
//        if ($validadns == 0) {
//            $valid = 0;
//            return 0;
//        }
        $validsubmask = $this->validateSubnetMask($net_mask);
        if ($validsubmask == 0) {
            $valid = 0;
            return 0;
        }
        $bcast = $this->checkBroadcast($net_ipaddr, $net_mask);
        $bcast1 = $this->checkBroadcast($net_gw, $net_mask);
        $comb = $this->checkIpCombination($net_ipaddr, $net_gw, $net_mask);
        //print_r("1::".$bcast."2::".$bcast1."3::".$comb);
        
        if (!$bcast || !$bcast1 || !$comb) {
            $valid = 0;
            return 0;
        }

        return $valid;
    }

}
