<?php

namespace Api\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Zend\Math\Rand;
use Zend\Session\Container;
use Zend\Http\Header\SetCookie;

class UserApiController extends AbstractRestfulController {

    /**
     * This function will be called in case of 
     * GET request from client
     * 
     * @return Response Json 
     */
    protected function getServiceInstance($service) {
        return $this->getServiceLocator()->get($service);
    }

    public function loginAction() {
        $response = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $user_email = $data['user_email'];
            $user_pwd = $data['user_pwd'];

            $validateModel = $this->getServiceInstance('Validate');
            $email_valid = $validateModel->emailValidate($user_email);
            $pwd_length_min = $validateModel->pwdValidate($user_pwd);

            if ($email_valid == 1 && $pwd_length_min == 1) {

                $userModel = $this->getServiceInstance('User');
                $request = $this->getRequest();
                $this->authService = $this->getServiceInstance('AuthService');
                $formData = Array();
                if (!empty($user_email) and ! empty($user_pwd)) {
                    $dbAdapter = $this->getServiceInstance('Zend\Db\Adapter\Adapter');
                    $userEntity = $userModel->getEntityInstance();
                    $userData = $userModel->validateLogin($data);
                    $userDataOtp = $userModel->validateLoginOtp($data);
                    if ($userDataOtp) {
                        if ($userDataOtp[0][0]['userStatus'] == 1) {
                            $session = new Container('base');
                            $session->offsetSet('user_id', $userDataOtp[0][0][userId]);
                            $resetOtp = $userModel->ResetOtp($userDataOtp[0][0][userId]);
                            $deviceModel = $this->getServiceInstance('Device');
                            $device_count = $deviceModel->getActiveDeviceCount($userData[0][0][userId]);
                            $status = true;
                            $message = "Set New Password";
                            $user_token = $userDataOtp[0][0]['userToken'];

                            $response['status'] = $status;
                            $response['message'] = $message;
                            $response['otp'] = 1;
                            $response['user_token'] = $user_token;
                            $response['device_count'] = $device_count;
                            $response = $this->getResponseWithHeader()->setContent(json_encode($response));
                            return $response;
                        }
                    }

                    if (empty($userData)) {
                        $status = false;
                        $message = "Incorrect Email or Password";
                        $user_token = "";
                    } else if ($userData[0][0]['userStatus'] == 0) {
                        $status = false;
                        $message = "Account is Not Activated";
                        $user_token = "";
                    } else if ($userData[0][0]['userStatus'] == 1) {
                        $session = new Container('base');
                        $session->offsetSet('user_id', $userData[0][0][userId]);
                        $session->offsetSet('user_email', $userData[0][0][userEmail]);
                        $deviceModel = $this->getServiceInstance('Device');
                        $device_count = $deviceModel->getActiveDeviceCount($userData[0][0][userId]);
                        $status = true;
                        $message = "Matching Data Found";
                        $otp = 0;
                        $user_token = $userData[0][0]['userToken'];
                        if (isset($data['app_token']) && isset($data['app_platform'])) {
                            $notifyModel = $this->getServiceInstance('PushNotification');
                            $getToken = $notifyModel->getAppToken($userData[0][0][userId], $data['app_token'], $data['app_platform'], $data['app_Identifier']);
                        }
                    }
                } else {
                    $status = false;
                    $message = "Email and password are mandatory in request";
                    $user_token = "";
                    $device_count = "";
                }
                $response['status'] = $status;
                $response['message'] = $message;
                $response['otp'] = $otp;
                $response['user_token'] = $user_token;
                $response['device_count'] = $device_count;
            } else {
                $status = false;
                $message = "Invalid data";
                $user_token = "";
                $device_count = "";
                $response['status'] = $status;
                $response['message'] = $message;
                $response['user_token'] = $user_token;
                $response['device_count'] = $device_count;
            }
        }


        $response = $this->getResponseWithHeader()
                ->setContent(json_encode($response));
        return $response;
    }

    public function registerAction() {
        $response = array();
        $userModel = $this->getServiceInstance('ApiOperations');
        $formData = array();
        $dbAdapter = $this->getServiceInstance('Zend\Db\Adapter\Adapter');
        //$data = $this->params()->fromPost();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);

            $validateModel = $this->getServiceInstance('Validate');
            $email_valid = $validateModel->emailValidate($data['user_email']);
            $pwd_valid = $validateModel->pwdValidate($data['user_pwd']);
            $name_valid = $validateModel->usernameValidate($data['user_name']);
            $mob_valid = $validateModel->MobValidate($data['user_mobile']);
            //print_r($name_valid . '@@' . $email_valid . '@@' . $pwd_valid . '@@' . $mob_valid);
            // die;
            if ($email_valid == 1 && $pwd_valid == 1) {

                $formData = $data;
                //Validate Email from whitelist
                $isValid = $this->validateEmailStatus($data['user_email']);
                $userModel = $this->getServiceInstance('User');
                $isUnique = $userModel->validateEmailUnique($data['user_email']);
                if ("true" === $isValid) {
                    if ("true" === $isUnique) {
                        // BEGIN-Create User
                        $userEntity = $userModel->getEntityInstance();
                        // change newline to comma address
                        $modelCommon = $this->getServiceInstance('Common');
                        $time = $modelCommon->getCurrentTimeStamp();
                        // Create Activation Key
                        $activationKey = $userModel->generateActivationKey($data);
                        $objectManager = $userModel->getObjectManager($this->getServiceLocator());
                        $userEntity->setUserName(htmlspecialchars($data['user_name']));
                        $enc_pwd = array();
                        $enc_pwd = $userModel->encryptPassword($data['user_pwd']);

                        $userEntity->setUserPwd($enc_pwd['key']);
                        $userEntity->setUserPwdsalt($enc_pwd['salt']);
                        $userEntity->setUserEmail(htmlspecialchars($data['user_email']));
                        $userEntity->setUserMobile(htmlspecialchars($data['user_mobile']));
                        //$userEntity->setemailConfirmation($activationKey);
                        $userEntity->setUserStatus(o); //0-> InActive Account
                        $userEntity->setUserRole(0);

                        $userModel->saveUser($userEntity);

                        $info["userId"] = $userEntity->getUserId();
                        $info["userName"] = $userEntity->getUserName();
                        $info["userEmail"] = $userEntity->getUserEmail();
                        $info["toEmail"] = $userEntity->getUserEmail();
                        $info["activationKey"] = $userModel->generateActivationKey($info);

                        $login_time = time();
                        $rand_num = Rand::getString(4, 'abcdefghijklmnopqrstuvwxyz0123456789', true);
                        $user_token = "s" . $info["userId"] . "" . $rand_num;
                        $userEntity->setUserToken($user_token);
                        $rand_num = Rand::getString(4, '0123456789', true);
                        $user_pin = $rand_num;
                        $userEntity->setUserPin($user_pin);
                        $userModel->saveUser($userEntity);

                        $userModel->sendEmail("registration", $info);
                        $status = true;
                        $message = "Account Created Successfully";
                    } else {
                        $status = false;
                        $message = "The email id is already registerd";
                    }
                } else {
                    $status = false;
                    $message = "Failed";
                }
            } else {
                $status = false;
                $message = "Invalid Data";
            }
        }
        $response['status'] = $status;
        $response['message'] = $message;
        $response = $this->getResponseWithHeader()
                ->setContent(json_encode($response));
        return $response;
    }

    public function profileAction() {
         $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        //print_r($user_id);
        if ($user_id) {

            $userModel = $this->getServiceInstance('User');
            $userData = $userModel->getDetails($user_id);
             //print_r($userData);die;
            $response['status'] = true;
            //$response['data'] = $userData;
            $response['user_token'] = $userData[0][userToken];
                $response['user_pin'] = $userData[0][userPin];
            //return new JsonModel($response);
        } else {
            $response['status'] = false;
            $response['message'] = "Session expired!";
            //return new JsonModel($response);
        }
        $response = $this->getResponseWithHeader()
                ->setContent(json_encode($response));
        return $response;
    }

    public function updatepushTokenAction() {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $data = json_decode(file_get_contents("php://input"), true);
            $token = $data['app_token'];
            $identifier = $data['app_Identifier'];
            $pushModel = $this->getServiceInstance('PushNotification');
            $update = $pushModel->updateTokenStatus($token, $identifier, $user_id);
            if ($update) {
                $response['status'] = true;
                $response['message'] = "Token Updated";
            } else {
                $response['status'] = false;
                $response['message'] = "Token not Updated";
            }
        } else {
            $response['message'] = false;
            $response['message'] = "Your Session Expired";
        }
        $response = $this->getResponseWithHeader()
                ->setContent(json_encode($response));
        return $response;
    }

    public function ForgotpasswordAction() {
        $response = array();
        $userModel = $this->getServiceInstance('User');
        $formData = array();
        $dbAdapter = $this->getServiceInstance('Zend\Db\Adapter\Adapter');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $formData = $data;
            $validateModel = $this->getServiceInstance('Validate');
            $email_valid = $validateModel->emailValidate($data['user_email']);
            if ($email_valid == 1) {
                $userCount = $userModel->getEmailCount($data['user_email']);
                if ($userCount == 1) {
                    $newPass = rand(78900000, 98700023);
                    $modelCommon = $this->getServiceInstance('Common');
                    $currentTS = $modelCommon->getCurrentTimeStamp();
                    $status = $userModel->resetPassword($data['user_email'], $newPass, 1, $currentTS);
                    if ($status) {
                        $response['status'] = true;
                        $user = $userModel->loadByUserEmail($data['user_email']);
                        $data['toEmail'] = $data['user_email'];
                        $data['userPwd'] = $newPass;
                        $data['userName'] = $user['userName'];
                        $userModel->sendEmail('passwordreset', $data);
                        $message = "Password reset successful!!We've sent a new password to your email address.";
                    } else {
                        $response['status'] = false;
                        $message = "Password reset failed";
                    }
                } else {
                    $response['status'] = false;
                    $message = "We couldn't find any record of the email address you entered";
                }
            } else {
                $response['status'] = false;
                $message = "Invalid Data";
            }
        }
        $response['message'] = $message;
        $response = $this->getResponseWithHeader()
                ->setContent(json_encode($response));
        return $response;
    }

    public function resetpwdAction() {
        $response = array();
        $userModel = $this->getServiceInstance('User');
        $formData = array();
        $dbAdapter = $this->getServiceInstance('Zend\Db\Adapter\Adapter');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = json_decode(file_get_contents("php://input"), true);
            $userModel = $this->getServiceInstance('User');
            $npwd = $data['user_pwd'];
            $oldpwd = $data['user_oldpwd'];
            $user_token = $data['user_token'];
            $validateModel = $this->getServiceInstance('Validate');
            $user_token = $validateModel->stripSpace($data['user_token']);
            $pwd_valid = $validateModel->pwdValidate($npwd);
            $oldpwd_valid = $validateModel->pwdValidate($oldpwd);

            if ($pwd_valid == 1 && $oldpwd_valid == 1) {
                $session = new Container('base');
                $id = $session->offsetGet('user_id');
                $aid = $session->offsetGet('adminuser_id');
                // print_r($aid);print_r($user_token);die;
                if ($id && $user_token) {

                    $pwdcheck = $userModel->validatepwd($oldpwd, $user_token);
                    if ($pwdcheck) {
                        $encPass = $userModel->encryptPassword($npwd);
                        $user = $userModel->selectdata($id);
                        $status = $userModel->resetpwd($user[0][0]['userId'], $encPass['key'], $encPass['salt']);
                        if ($status) {
                            $response['status'] = true;
                            $response['message'] = "Password reset successfully.Please Login Again";
                        } else {
                            $response['status'] = false;
                            $response['message'] = "Password reset failed";
                        }
                    } else {
                        $response['status'] = false;
                        $response['message'] = "Current password is wrong";
                    }
                } elseif ($aid && $user_token) {

                    $pwdcheck = $userModel->validatepwd($oldpwd, $user_token);
                    if ($pwdcheck) {
                        $encPass = $userModel->encryptPassword($npwd);
                        $user = $userModel->selectdata($aid);
                        $status = $userModel->resetpwd($user[0][0]['userId'], $encPass['key'], $encPass['salt']);
                        if ($status) {
                            $response['status'] = true;
                            $response['message'] = "Password reset successfully.Please Login Again";
                        } else {
                            $response['status'] = false;
                            $response['message'] = "Password reset failed";
                        }
                    } else {
                        $response['status'] = false;
                        $response['message'] = "Current password is wrong";
                    }
                } else {
                    $response['status'] = false;
                    $response['message'] = "Session expired!";
                }
            } else {
                $response['status'] = false;
                $response['message'] = "Invalid Data";
            }
            $response = $this->getResponseWithHeader()->setContent(json_encode($response));
            return $response;
        }
    }

    public function logoutAction() {

        $session_user = new Container('base');
        $user_id = $session_user->offsetGet('adminuser_id');
        $id = $session_user->offsetGet('user_id');

        if ($user_id || $id) {
            $session_user->getManager()->getStorage()->clear();
            $response['result'] = [0];
            return new JsonModel($response);
        } else {
            $error = 1;
            $message = "No Session Found";
        }
        return new JsonModel(array("error" => [$error, "$message"]));
    }

    public function getResponseWithHeader() {
        $response = $this->getResponse();
        $response->getHeaders()
                //make can accessed by *  
                ->addHeaderLine('Access-Control-Allow-Origin', '*')
                //set allow methods
                ->addHeaderLine('Access-Control-Allow-Methods', 'POST PUT DELETE GET');
        return $response;
    }

    public function validateEmailStatus($email) {
        $isValid = "false";
        $config = $this->getServiceInstance('config');
        // Allowed
        $allowedDomains = $config['email_whitelist']['domains'];
        $allowedEmails = $config['email_whitelist']['emailids'];
        // Not Allowed
        $notAllowedDomains = $config['email_blacklist']['domains'];
        $notAllowedEmails = $config['email_blacklist']['emailids'];
        // Validation Status
        $validateStatus = $config['email_validate_check'];
        $emailDomain = substr(strrchr($email, "@"), 1);
        // Check for whitelisted data
        if ($validateStatus['whitelist']) {
            if (in_array($emailDomain, $allowedDomains)) {
                return $isValid = "true";
            } elseif (in_array($email, $config['email_whitelist']['emailids'])) {
                return $isValid = "true";
            } elseif (empty($allowedDomains)) {
                return $isValid = "true";
            } else {
                foreach ($config['email_whitelist']['patterns'] as $pattern => $patternLimit) {
                    $emailParts = explode("#range#", $pattern);
                    $isMatch = preg_match('/^' . $emailParts[0] . '(\d+)' . $emailParts[1] . '$/', $email, $matches);
                    if ($isMatch && $matches[1] >= $patternLimit['startLimit'] && $matches[1] <= $patternLimit['endLimit']) {
                        return $isValid = "true";
                        break;
                    }
                }
            }
        }
    }

}
