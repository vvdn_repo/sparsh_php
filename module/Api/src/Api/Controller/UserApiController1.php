<?php



namespace Api\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Zend\Math\Rand;
use Zend\Session\Container;
use Zend\Http\Header\SetCookie;

class UserApiController extends AbstractRestfulController {

    
    /**
     * This function will be called in case of 
     * GET request from client
     * 
     * @return Response Json 
     */
    protected function getServiceInstance($service) {
        return $this->getServiceLocator()->get($service);
    }
    
    public function loginAction() {
        
        $response = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
       $data  = json_decode (file_get_contents ("php://input"), true);
       $user_email = $data['user_email'];
       $user_pwd = $data['user_pwd'];
       $isAdmin = $data['isAdmin'];
       //print_r($data['isAdmin']);die;
       if($isAdmin == '0' || !isAdmin)
       {   // print_r('user');die;
        $userModel = $this->getServiceInstance('User');
        $request = $this->getRequest();
        $this->authService = $this->getServiceInstance('AuthService');
        $formData=Array();
        if(!empty($user_email) and !empty($user_pwd)) {
             $dbAdapter = $this->getServiceInstance('Zend\Db\Adapter\Adapter');  
             $userEntity = $userModel->getEntityInstance();
             $userData=$userModel->validateLogin($data);
             $userDataOtp=$userModel->validateLoginOtp($data);
            if($userDataOtp)
            {
                if($userDataOtp[0][0]['userStatus'] == 1) {
                    $session = new Container('base');
                    $session->offsetSet('user_id',$userDataOtp[0][0][userId]);
                    $deviceModel = $this->getServiceInstance('Device');
                    $device_count = $deviceModel->getActiveDeviceCount($userData[0][0][userId]);
                    $status = true;
                    $message = "Set New Password";
                    $user_token = $userDataOtp[0][0]['userToken'];

                    $response['status'] = $status;
                    $response['message'] = $message;
                    $response['user_token'] = $user_token;
                $response['device_count'] = $device_count;
                    $response = $this->getResponseWithHeader() ->setContent(json_encode($response));
                    return $response;
                }
            }

             if (empty($userData)) {
                $status = false;
                 $message = "Incorrect Email or Password";
                $user_token = "";
             }
            else if($userData[0][0]['userStatus'] == 0) {
                $status = false;
                 $message = "Account is Not Activated";
                $user_token = "";
             }
            else if($userData[0][0]['userStatus'] == 1) {
                $session = new Container('base');
                $session->offsetSet('user_id',$userData[0][0][userId]);
                $session->offsetSet('user_email',$userData[0][0][userEmail]);
                $deviceModel = $this->getServiceInstance('Device');
                $device_count = $deviceModel->getActiveDeviceCount($userData[0][0][userId]);
                $status = true;
                $message = "Matching Data Found";
                $user_token = $userData[0][0]['userToken'];
                if (isset($data['app_token']) && isset($data['app_platform'])) {
                         $notifyModel = $this->getServiceInstance('PushNotification');
                         $getToken = $notifyModel->getAppToken($userData[0][0][userId],$data['app_token'],$data['app_platform'],$data['app_Identifier']);
                 }
             }


         } else {
             $status = false;
             $message = "Email and password are mandatory in request";
             $user_token = "";
             $device_count = "";
         }
         $response['status'] = $status;
        $response['message'] = $message;
        $response['user_token'] = $user_token;
        $response['device_count'] = $device_count;
         }
        
        elseif ($isAdmin == 1) {
            //print_r('admin true');die;
            $adminModel = $this->getServiceInstance('ApiAdmin');
            $request = $this->getRequest();
            $this->authService = $this->getServiceInstance('AuthService');
            $formData=Array();
            if(!empty($user_email) and !empty($user_pwd)) {
                 $dbAdapter = $this->getServiceInstance('Zend\Db\Adapter\Adapter');  
                 $adminEntity = $adminModel->getEntityInstance();
                 $adminData=$adminModel->validateLogin($data);
                 $adminDataOtp=$adminModel->validateLoginOtp($data);
                // print_r($adminData);die;
                if($adminDataOtp)
                {
                    //print_r($adminDataOtp);die;$userDataOtp
                    if($adminDataOtp[0][0]['adminStatus'] == 1) {
                        $session = new Container('base');
                        $session->offsetSet('admin_id',$adminDataOtp[0][0][adminId]);
                        $deviceModel = $this->getServiceInstance('Device');
                        $device_count = $deviceModel->getActiveDeviceCount($userData[0][0][userId]);
                        $status = true;
                        $message = "Set New Password";
                        $admin_token = $adminDataOtp[0][0]['adminToken'];

                        $response['status'] = $status;
                        $response['message'] = $message;
                        $response['user_token'] = $admin_token;
                    $response['device_count'] = $device_count;
                        $response = $this->getResponseWithHeader() ->setContent(json_encode($response));
                        return $response;
                    }
                }

                 if (empty($adminData)) {
                    $status = false;
                     $message = "Incorrect Email or Password";
                    $user_token = "";
                 }
                else if($adminData[0][0]['adminStatus'] == 0) {
                    $status = false;
                     $message = "Account is Not Activated";
                    $user_token = "";
                 }
                else if($adminData[0][0]['adminStatus'] == 1) {
                    $session = new Container('base');
                    $session->offsetSet('admin_id',$userData[0][0][adminId]);
                    $session->offsetSet('admin_email',$userData[0][0][adminEmail]);
                    $deviceModel = $this->getServiceInstance('Device');
                    $device_count = $deviceModel->getActiveDeviceCount($adminData[0][0][adminId]);
                    $status = true;
                    $message = "Matching Data Found";
                    $admin_token = $adminData[0][0]['adminToken'];
                    if (isset($data['app_token']) && isset($data['app_platform'])) {
                             $notifyModel = $this->getServiceInstance('PushNotification');
                             $getToken = $notifyModel->getAppToken($adminData[0][0][adminId],$data['app_token'],$data['app_platform'],$data['app_Identifier']);
                     }
                 }


             } else {
                 $status = false;
                 $message = "Email and password are mandatory in request";
                 $user_token = "";
                 $device_count = "";
             }
                $response['status'] = $status;
                $response['message'] = $message;
                $response['admin_token'] = $admin_token;
                $response['device_count'] = $device_count;

            }
        }
        

        $response = $this->getResponseWithHeader()
                ->setContent(json_encode($response));
        return $response;
    }
    public function registerAction() {
        $response = array();
        $userModel = $this->getServiceInstance('ApiOperations');
        $formData = array();
        $dbAdapter = $this->getServiceInstance('Zend\Db\Adapter\Adapter');
        //$data = $this->params()->fromPost();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = json_decode (file_get_contents ("php://input"), true);

            $formData = $data;
            //Validate Email from whitelist
            $isValid = $this->validateEmailStatus($data['user_email']);
            $userModel = $this->getServiceInstance('User');
            $isUnique =$userModel->validateEmailUnique($data['user_email']);
            if ("true" === $isValid ) {
                if ("true" === $isUnique ) {
                    // BEGIN-Create User
                    $userEntity = $userModel->getEntityInstance();
                    // change newline to comma address
                    $modelCommon = $this->getServiceInstance('Common');
                    $time = $modelCommon->getCurrentTimeStamp();
                    // Create Activation Key
                    $activationKey = $userModel->generateActivationKey($data);
                    $objectManager = $userModel->getObjectManager($this->getServiceLocator());
                    $userEntity->setUserName(htmlspecialchars($data['user_name']));
                    $enc_pwd = array();
                    $enc_pwd = $userModel->encryptPassword($data['user_pwd']);

                    $userEntity->setUserPwd($enc_pwd['key']);
                    $userEntity->setUserPwdsalt($enc_pwd['salt']);
                    $userEntity->setUserEmail(htmlspecialchars($data['user_email']));
                    $userEntity->setUserMobile(htmlspecialchars($data['user_mobile']));
                    //$userEntity->setemailConfirmation($activationKey);
                    $userEntity->setUserStatus(o); //0-> InActive Account
                    $userEntity->setUserRole(0);
		   
                    $userModel->saveUser($userEntity);

                    $info["userId"] = $userEntity->getUserId();
                    $info["userName"] = $userEntity->getUserName();
                    $info["userEmail"] = $userEntity->getUserEmail();
                    $info["toEmail"] = $userEntity->getUserEmail();
                    $info["activationKey"] = $userModel->generateActivationKey($info);

                    $login_time=time();
                    $rand_num= Rand::getString(5,'abcdefghijklmnopqrstuvwxyz0123456789', true);
                    $user_tocken="sb_".$info["userId"]."_".$rand_num;
                    $userEntity->setUserToken($user_tocken);
                    $userModel->saveUser($userEntity);
		    

 		    

                    $userModel->sendEmail("registration", $info);
                    $status =true;
                    $message = "Account Created Successfully";
                }
                else{
                    $status =false;
                    $message = "The email id is already registerd";
                }
            }
            else {
                $status =false;
                $message = "Failed";
            }
        }
        $response['status'] = $status;
        $response['message'] = $message;
        $response = $this->getResponseWithHeader()
            ->setContent(json_encode($response));
        return $response;
    }


    public function updatepushTokenAction()
    {
        $session = new Container('base');
        $user_id = $session->offsetGet('user_id');
        if ($user_id) {
            $response = array();
            $request = $this->getRequest();
            $data = json_decode(file_get_contents("php://input"), true);
            $token = $data['app_token'];
            $identifier = $data['app_Identifier'];
            $pushModel = $this->getServiceInstance('PushNotification');
            $update = $pushModel->updateTokenStatus($token,$identifier,$user_id);
            if($update){
                $response['status'] = true;
                $response['message']= "Token Updated";
                }
            else{
                    $response['status'] = false;
                     $response['message'] = "Token not Updated";
                }
        }
        else{
             $response['message'] = false;
             $response['message'] = "Your Session Expired";
        }
         $response = $this->getResponseWithHeader()
            ->setContent(json_encode($response));
        return $response;
    }


    public function ForgotpasswordAction() {
        $response = array();
        $userModel = $this->getServiceInstance('User');
        $formData = array();
        $dbAdapter = $this->getServiceInstance('Zend\Db\Adapter\Adapter');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = json_decode (file_get_contents ("php://input"), true);
            $formData = $data;
            $userCount = $userModel->getEmailCount($data['user_email']);
            if ($userCount == 1) {
                $newPass = rand(78900000, 98700023);
                $modelCommon = $this->getServiceInstance('Common');
                $currentTS = $modelCommon->getCurrentTimeStamp();
                $status = $userModel->resetPassword($data['user_email'], $newPass, 1, $currentTS);
                if ($status) {
                    $response['status'] = true;
                    $user = $userModel->loadByUserEmail($data['user_email']);
                    $data['toEmail'] = $data['user_email'];
                    $data['userPwd'] = $newPass;
                    $data['userName'] = $user['userName'];
                    $userModel->sendEmail('passwordreset', $data);
                    $message= "Password reset successful!!We've sent a new password to your email address.";
                }
                else{
                    $response['status'] = false;
                    $message = "Password reset failed";
                }
            }
            else{
                $response['status'] = false;
                $message = "We couldn't find any record of the email address you entered";
            }
        }
        $response['message'] = $message;
        $response = $this->getResponseWithHeader()
            ->setContent(json_encode($response));
        return $response;
    }
 

    public function resetpwdAction()
    {
         $response = array();
        $userModel = $this->getServiceInstance('User');
        $formData = array();
        $dbAdapter = $this->getServiceInstance('Zend\Db\Adapter\Adapter');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = json_decode (file_get_contents ("php://input"), true);
            $userModel = $this->getServiceInstance('User');
            $npwd = $data['user_pwd'];
	    $user_token = $data['user_token'];
            $session = new Container('base');
            $id = $session->offsetGet('user_id');
	    if($id && $user_token){
            $encPass = $userModel->encryptPassword($npwd);
            $user = $userModel->selectdata($id);
            $status = $userModel->resetpwd($user[0][0]['userId'], $encPass['key'],$encPass['salt']);
                if ($status) {
                    $response['status'] = true;
                    $response['message'] = "Password reset successfully.Please Login Again";
                }
                else {
                    $response['status'] = false;
                    $response['message'] = "Password reset failed";
                }
            }
           else{
		    $response['status'] = false;
                    $response['message'] = "Session expired!";
           }
	$response = $this->getResponseWithHeader() ->setContent(json_encode($response));
        return $response;
        }
    }

    public function logoutAction() {
        $request = $this->getRequest();
        $data = json_decode (file_get_contents ("php://input"), true);
        $session_user = new Container('base');
        $session_user->getManager()->getStorage()->clear();
        $pushModel = $this->getServiceLocator()->get('PushNotification');
        $logout = $pushModel->updateLogout($data['app_platform'],$data['app_Identifier']);
        if($logout){
            $response['status'] = true;
            $response['message'] = "User Session Destroyed";
            return new JsonModel($response);
        }
        $response['status'] = false;
        return new JsonModel($response);
    }


public function getResponseWithHeader() {
        $response = $this->getResponse();
        $response->getHeaders()
                //make can accessed by *  
                ->addHeaderLine('Access-Control-Allow-Origin', '*')
                //set allow methods
                ->addHeaderLine('Access-Control-Allow-Methods', 'POST PUT DELETE GET');
        return $response;
    }

    public function validateEmailStatus($email) {
        $isValid = "false";
        $config = $this->getServiceInstance('config');
        // Allowed
        $allowedDomains = $config['email_whitelist']['domains'];
        $allowedEmails = $config['email_whitelist']['emailids'];
        // Not Allowed
        $notAllowedDomains = $config['email_blacklist']['domains'];
        $notAllowedEmails = $config['email_blacklist']['emailids'];
        // Validation Status
        $validateStatus = $config['email_validate_check'];
        $emailDomain = substr(strrchr($email, "@"), 1);
        // Check for whitelisted data
        if ($validateStatus['whitelist']) {
            if (in_array($emailDomain, $allowedDomains)) {
                return $isValid = "true";
            } elseif (in_array($email, $config['email_whitelist']['emailids'])) {
                return  $isValid = "true";
            } elseif (empty($allowedDomains)) {
                return  $isValid = "true";
            } else {
                foreach ($config['email_whitelist']['patterns'] as $pattern => $patternLimit) {
                    $emailParts = explode("#range#", $pattern);
                    $isMatch = preg_match('/^' . $emailParts[0] . '(\d+)' . $emailParts[1] . '$/', $email, $matches);
                    if ($isMatch && $matches[1] >= $patternLimit['startLimit'] && $matches[1] <= $patternLimit['endLimit']) {
                        return $isValid = "true";
                        break;
                    }
                }
            }
        }

    }

}
