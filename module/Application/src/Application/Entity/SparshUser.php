<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SparshUser
 *
 * @ORM\Table(name="sparsh_user", uniqueConstraints={@ORM\UniqueConstraint(name="user_email_UNIQUE", columns={"user_email"})})
 * @ORM\Entity
 */
class SparshUser{

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="user_name", type="string", length=45, nullable=true)
     */
    private $userName;

    /**
     * @var string
     *
     * @ORM\Column(name="user_email", type="string", length=100, nullable=true)
     */
    private $userEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="user_pwd", type="string", length=100, nullable=true)
     */
    private $userPwd;

    /**
     * @var string
     *
     * @ORM\Column(name="user_pwdsalt", type="string", length=500, nullable=true)
     */
    private $userPwdsalt;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_createdby_fk", type="integer", nullable=true)
     */
    private $userCreatedbyFk;

    /**
     * @var string
     *
     * @ORM\Column(name="user_mobile", type="string", length=20, nullable=true)
     */
    private $userMobile;


    /**
     * @var integer
     *
     * @ORM\Column(name="user_status", type="integer", nullable=true)
     */
    private $userStatus;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="user_role", type="integer", nullable=true)
     */
    private $userRole;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_token", type="string",length=100, nullable=true)
     */
    private $userToken;
    /**
     * @var integer
     *
     * @ORM\Column(name="user_pin", type="string",length=100, nullable=true)
     */
    private $userPin;
    /**
     * @var boolean
     *
     * @ORM\Column(name="user_otpstatus", type="integer", nullable=true)
     */
    private $userOtpstatus;

    /**
     * @var string
     *
     * @ORM\Column(name="user_otp", type="string", length=100, nullable=true)
     */
    private $userOtp;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_otptimestamp", type="integer", nullable=true)
     */
    private $userOtpTimestamp;


    
    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId() {
        return $this->userId;
    }

    /**
     * Set userName
     *
     * @param string $userName
     * @return integer
     */
    public function setUserName($userName) {
        $this->userName = $userName;

        return $this;
    }

    /**
     * Get userName
     *
     * @return string 
     */
    public function getUserName() {
        return $this->userName;
    }

    /**
     * Set userEmail
     *
     * @param string $userEmail
     * @return string
     */
    public function setUserEmail($userEmail) {
        $this->userEmail = $userEmail;

        return $this;
    }

    /**
     * Get userEmail
     *
     * @return string 
     */
    public function getUserEmail() {
        return $this->userEmail;
    }

    /**
     * Set userPwd
     *
     * @param string $userPwd
     * @return string
     */
    public function setUserPwd($userPwd) {
        $this->userPwd = $userPwd;

        return $this;
    }

    /**
     * Get userPwd
     *
     * @return string 
     */
    public function getUserPwd() {
        return $this->userPwd;
    }


    /**
     * Set userPwdsalt
     *
     * @param string $userPwdsalt
     * @return string
     */
    public function setUserPwdsalt($userPwdsalt) {
        $this->userPwdsalt = $userPwdsalt;

        return $this;
    }

    /**
     * Get userPwdsalt
     *
     * @return string
     */
    public function getUserPwdsalt() {
        return $this->userPwdsalt;
    }



    /**
     * Set userCreatedbyFk
     *
     * @param integer $userCreatedbyFk
     * @return string
     */
    public function setUserCreatedbyFk($userCreatedbyFk) {
        $this->userCreatedbyFk = $userCreatedbyFk;

        return $this;
    }

    /**
     * Get userCreatedbyFk
     *
     * @return integer 
     */
    public function getUserCreatedbyFk() {
        return $this->userCreatedbyFk;
    }


    /**
     * Set userMobile
     *
     * @param string $userMobile
     * @return string
     */
    public function setUserMobile($userMobile) {
        $this->userMobile = $userMobile;

        return $this;
    }

    /**
     * Get userMobile
     *
     * @return string 
     */
    public function getUserMobile() {
        return $this->userMobile;
    }

    /**
     * Set userStatus
     *
     * @param integer $userStatus
     * @return string
     */
    public function setUserStatus($userStatus) {
        $this->userStatus = $userStatus;

        return $this;
    }

    /**
     * Get userStatus
     *
     * @return integer
     */
    public function getUserStatus() {
        return $this->userStatus;
    }
    /**
     * Set userRole
     *
     * @param integer $userRole
     * @return string
     */
    public function setUserRole($userRole) {
        $this->userRole = $userRole;

        return $this;
    }

    /**
     * Get userRole
     *
     * @return integer
     */
    public function getUserRole() {
        return $this->userRole;
    }



     /**
 * Set userToken
 *
 * @param integer $userToken
 * @return string
 */
    public function setUserToken($userTocken) {
        $this->userToken = $userTocken;

        return $this;
    }

    /**
     * Get userToken
     *
     * @return string
     */
    public function getUserToken() {
        return $this->userToken;
    }
    
     /**
 * Set userPin
 *
 * @param integer $userPin
 * @return string
 */
    public function setUserPin($userPin) {
        $this->userPin = $userPin;

        return $this;
    }

    /**
     * Get userToken
     *
     * @return string
     */
    public function getUserPin() {
        return $this->userPin;
    }
    

 /**
     * Set userOtpstatus
     *
     * @param boolean $userOtpstatus
     * @return SparshUser
     */
    public function setUserOtpstatus($userOtpstatus) {
        $this->userOtpstatus = $userOtpstatus;

        return $this;
    }

    /**
     * Get userOtpstatus
     *
     * @return boolean 
     */
    public function getUserOtpstatus() {
        return $this->userOtpstatus;
    }

    /**
     * Set userOtp
     *
     * @param string $userOtp
     * @return SparshUser
     */
    public function setUserOtp($userOtp) {
        $this->userOtp = $userOtp;

        return $this;
    }

    /**
     * Get userOtp
     *
     * @return string 
     */
    public function getUserOtp() {
        return $this->userOtp;
    }

    /**
     * Set userOtpTimestamp
     *
     * @param integer $userOtpTimestamp
     * @return SparshUser
     */
    public function setUserOtpTimestamp($userOtpTimestamp) {
        $this->userOtpTimestamp = $userOtpTimestamp;

        return $this;
    }

    /**
     * Get userOtpTimestamp
     *
     * @return integer 
     */
    public function getUserOtpTimestamp() {
        return $this->userOtpTimestamp;
    }

}
