<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SparshDevice
 *
 * @ORM\Table(name="sparsh_device", indexes={@ORM\Index(name="device_createdby", columns={"device_createdby_fk"})})
 * @ORM\Entity
 */
class SparshDevice {

    /**
     * @var integer
     *
     * @ORM\Column(name="device_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $deviceId;

    /**
     * @var string
     *
     * @ORM\Column(name="device_name", type="string", length=100, nullable=true)
     */
    private $deviceName;

    /**
     * @var string
     *
     * @ORM\Column(name="device_type", type="string",length=100, nullable=true)
     */
    private $deviceType;

    /**
     * @var string
     *
     * @ORM\Column(name="device_mac", type="string", length=45, nullable=true)
     */
    private $deviceMac;

    /**
     * @var integer
     *
     * @ORM\Column(name="device_createdon", type="integer", nullable=true)
     */
    private $deviceCreatedon;

    /**
     * @var integer
     *
     * @ORM\Column(name="device_status", type="integer", nullable=true)
     */
    private $deviceStatus = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="device_slnumber", type="string", length=45, nullable=true)
     */
    private $deviceSlnumber;

    /**
     * @var \Application\Entity\SparshUser
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\SparshUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="device_createdby_fk", referencedColumnName="user_id", nullable=true)
     * })
     */
    private $deviceCreatedbyFk;

   

    /**
     * @var string
     *
     * @ORM\Column(name="device_token", type="string", length=45, nullable=true)
     */
    private $deviceToken;

    /**
     * @var string
     *
     * @ORM\Column(name="device_IP", type="string", length=100, nullable=true)
     */
    private $deviceIP;

    /**
     * @var integer
     *
     * @ORM\Column(name="device_default", type="integer", options={"default" = 0})
     */
    private $deviceDefault;

    /**
     * @var string
     *
     * @ORM\Column(name="device_FW", type="string",length=200,nullable=true,options={"default"="2.1.3.423"})
     */
    private $deviceFW;

    /**
     * @var string
     *
     * @ORM\Column(name="device_config", type="string", length=10000, nullable=true)
     */
    private $deviceConfig;

    /**
     * @var string
     *
     * @ORM\Column(name="device_wifi_settings", type="string", length=1000, nullable=true)
     */
    private $deviceWifiSettings;
    
    /**
     * @var string
     *
     * @ORM\Column(name="device_ethernet_settings", type="string", length=1000, nullable=true)
     */
    private $deviceEthernetSettings;

    /**
     * @var integer
     *
     * @ORM\Column(name="device_viewer_app_setting", type="integer", options={"default" = 0})
     */
    private $deviceViewerAppSetting;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="device_live_video_cloud", type="integer", options={"default" = 0})
     */
    private $deviceLiveVideoCloud;

    /**
     * Constructor
     */
    public function __construct() {
        $this->sparshAp = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sparshDeviceSite = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get deviceId
     *
     * @return integer
     */
    public function getDeviceId() {
        return $this->deviceId;
    }

    /**
     * Set deviceName
     *
     * @param string $deviceName
     * @return SparshDevice
     */
    public function setDeviceName($deviceName) {
        $this->deviceName = $deviceName;

        return $this;
    }

    /**
     * Get deviceName
     *
     * @return string
     */
    public function getDeviceName() {
        return $this->deviceName;
    }

    /**
     * Set deviceType
     *
     * @param string $deviceType
     * @return SparshDevice
     */
    public function setDeviceType($deviceType) {
        $this->deviceType = $deviceType;

        return $this;
    }

    /**
     * Get deviceType
     *
     * @return string
     */
    public function getDeviceType() {
        return $this->deviceType;
    }

    /**
     * Set deviceMac
     *
     * @param string $deviceMac
     * @return SparshDevice
     */
    public function setDeviceMac($deviceMac) {
        $this->deviceMac = $deviceMac;

        return $this;
    }

    /**
     * Get deviceMac
     *
     * @return string
     */
    public function getDeviceMac() {
        return $this->deviceMac;
    }

    /**
     * Set deviceCreatedon
     *
     * @param integer $deviceCreatedon
     * @return SparshDevice
     */
    public function setDeviceCreatedon($deviceCreatedon) {
        $this->deviceCreatedon = $deviceCreatedon;

        return $this;
    }

    /**
     * Get deviceCreatedon
     *
     * @return integer
     */
    public function getDeviceCreatedon() {
        return $this->deviceCreatedon;
    }

    /**
     * Set deviceStatus
     *
     * @param boolean $deviceStatus
     * @return SparshDevice
     */
    public function setDeviceStatus($deviceStatus) {
        $this->deviceStatus = $deviceStatus;

        return $this;
    }

    /**
     * Get deviceStatus
     *
     * @return boolean
     */
    public function getDeviceStatus() {
        return $this->deviceStatus;
    }

    /**
     * Set deviceSlnumber
     *
     * @param string $deviceSlnumber
     * @return SparshDevice
     */
    public function setDeviceSlnumber($deviceSlnumber) {
        $this->deviceSlnumber = $deviceSlnumber;

        return $this;
    }

    /**
     * Get deviceSlnumber
     *
     * @return string
     */
    public function getDeviceSlnumber() {
        return $this->deviceSlnumber;
    }

    /**
     * Set deviceCreatedbyFk
     *
     * @param \Application\Entity\SparshDevice $deviceCreatedbyFk
     * @return SparshDevice
     */
    public function setDeviceCreatedbyFk(\Application\Entity\SparshUser $deviceCreatedbyFk = null) {
        $this->deviceCreatedbyFk = $deviceCreatedbyFk;

        return $this;
    }

    /**
     * Get deviceCreatedbyFk
     *
     * @return \Application\Entity\SparshDevice
     */
    public function getDeviceCreatedbyFk() {
        return $this->deviceCreatedbyFk;
    }

   

    /**
     * Set deviceToken
     *
     * @param string $deviceToken
     * @return SparshDevice
     */
    public function setDeviceToken($deviceToken) {
        $this->deviceToken = $deviceToken;

        return $this;
    }

    /**
     * Get deviceToken
     *
     * @return string
     */
    public function getDeviceToken() {
        return $this->deviceToken;
    }

    /**
     * Set deviceIP
     *
     * @param string $deviceIP
     * @return SparshDevice
     */
    public function setDeviceIP($deviceIP) {
        $this->deviceIP = $deviceIP;

        return $this;
    }

    /**
     * Get deviceIP
     *
     * @return string
     */
    public function getDeviceIP() {
        return $this->deviceIP;
    }

    /**
     * Set deviceDefault
     *
     * @param integer $deviceDefault
     * @return SparshDevice
     */
    public function setDeviceDefault($deviceDefault) {
        $this->deviceDefault = $deviceDefault;

        return $this;
    }

    /**
     * Get deviceDefault
     *
     * @return integer
     */
    public function getdeviceDefault() {
        return $this->deviceDefault;
    }

    /**
     * Set deviceFW
     *
     * @param string $deviceFW
     * @return SparshDevice
     */
    public function setDeviceFW($deviceFW) {
        $this->deviceFW = $deviceFW;

        return $this;
    }

    /**
     * Get deviceFW
     *
     * @return string
     */
    public function getDeviceFW() {
        return $this->deviceFW;
    }

    /**
     * Set deviceConfig
     *
     * @param string $deviceConfig
     * @return SparshDevice
     */
    public function setDeviceConfig($deviceConfig) {
        $this->deviceConfig = $deviceConfig;

        return $this;
    }

    /**
     * Get deviceConfig
     *
     * @return string
     */
    public function getDeviceConfig() {
        return $this->deviceConfig;
    }

    /**
     * Set deviceWifiSettings
     *
     * @param string $deviceWifiSettings
     * @return SparshDevice
     */
    public function setDeviceWifiSettings($deviceWifiSettings) {
        $this->deviceWifiSettings = $deviceWifiSettings;

        return $this;
    }

    /**
     * Get deviceEthernetSettings
     *
     * @return string
     */
    public function getDeviceWifiSettings() {
        return $this->deviceWifiSettings;
    }
   

    /**
     * Set deviceViewerAppSetting
     *
     * @param integer $deviceViewerAppSetting
     * @return SparshDevice
     */
    public function setDeviceViewerAppSetting($deviceViewerAppSetting) {
        $this->deviceViewerAppSetting = $deviceViewerAppSetting;

        return $this;
    }

    /**
     * Get deviceViewerAppSetting
     *
     * @return integer
     */
    public function getDeviceViewerAppSetting() {
        return $this->deviceViewerAppSetting;
    }
    /**
     * Set deviceLiveVideoCloud
     *
     * @param integer $deviceLiveVideoCloud
     * @return SparshDevice
     */
    public function setDeviceLiveVideoCLoud($deviceLiveVideoCloud) {
        $this->deviceLiveVideoCloud = $deviceLiveVideoCloud;

        return $this;
    }

    /**
     * Get deviceLiveVideoCloud
     *
     * @return integer
     */
    public function getDeviceLiveVideoCLoud() {
        return $this->deviceLiveVideoCloud;
    }
    /**
     * Set deviceEthernetSettings
     *
     * @param string $deviceEthernetSettings
     * @return SparshDevice
     */
    public function setDeviceEthernetSettings($deviceEthernetSettings) {
        $this->deviceEthernetSettings = $deviceEthernetSettings;

        return $this;
    }

    /**
     * Get deviceEthernetSettings
     *
     * @return string
     */
    public function getDeviceEthernetSettings() {
        return $this->deviceEthernetSettings;
    }
}
